package org.xbdframework.jms;

import org.springframework.messaging.MessageHeaders;

public interface XbdMessageListener<T> {

	void handleMessage(T message, MessageHeaders headers, javax.jms.Message jmsMessage,
			javax.jms.Session session);

}
