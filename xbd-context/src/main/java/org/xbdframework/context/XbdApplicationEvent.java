package org.xbdframework.context;

import org.springframework.context.ApplicationEvent;

public abstract class XbdApplicationEvent extends ApplicationEvent {

	public XbdApplicationEvent(Object source) {
		super(source);
	}

	@Override
	public String toString() {
		return "XbdApplicationEvent{" + "source=" + source + '}';
	}

}
