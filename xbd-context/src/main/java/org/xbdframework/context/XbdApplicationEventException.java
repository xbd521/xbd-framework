package org.xbdframework.context;

public abstract class XbdApplicationEventException extends RuntimeException {

	private XbdApplicationEvent event;

	public XbdApplicationEventException(String message) {
		super(message);
	}

	public XbdApplicationEventException(String message, Throwable cause) {
		super(message, cause);
	}

	public XbdApplicationEvent getEvent() {
		return this.event;
	}

	public void setEvent(XbdApplicationEvent event) {
		this.event = event;
	}

}
