package org.xbdframework.context;

/**
 * {@code XbdApplicationEvent}事件发布接口.
 *
 * @param <E> {@code XbdApplicationEvent}具体事件子类
 * @author luas
 * @since 4.3
 */
@FunctionalInterface
public interface XbdApplicationEventPublisher<E extends XbdApplicationEvent> {

	/**
	 * 发布事件.
	 * @param e {@code XbdApplicationEvent}具体事件子类
	 */
	void publishEvent(E e);

}
