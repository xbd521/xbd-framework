package org.xbdframework.context.support;

import org.xbdframework.context.XbdApplicationEventException;

public class XbdApplicationEventListenerUnexpectedException
		extends XbdApplicationEventException {

	public XbdApplicationEventListenerUnexpectedException(String message) {
		super(message);
	}

	public XbdApplicationEventListenerUnexpectedException(String message,
			Throwable cause) {
		super(message, cause);
	}

}
