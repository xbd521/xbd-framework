package org.xbdframework.errorhandler;

import org.springframework.util.ReflectionUtils;

public class ThrowExceptionErrorHandler extends LoggingErrorHandler {

	@Override
	public void handleError(Throwable t) {
		super.handleError(t);

		ReflectionUtils.rethrowRuntimeException(t);
	}

}
