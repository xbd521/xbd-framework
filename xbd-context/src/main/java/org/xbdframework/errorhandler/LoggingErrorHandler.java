package org.xbdframework.errorhandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.util.ErrorHandler;

public class LoggingErrorHandler implements ErrorHandler {

	protected final Logger logger = LoggerFactory.getLogger(LoggingErrorHandler.class);

	@Override
	public void handleError(Throwable t) {
		if (this.logger.isErrorEnabled()) {
			this.logger.error("系统发生异常，", t);
		}
	}

}
