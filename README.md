# xbd-framework

基于Spring Framework的自研Framework框架

## 与Spring Framework版本关系

从Spring Framework 4.3开始，之前的版本不在处理，与Spring Framework版本保持一致

<table>
    <tr>
        <td>xbd-framework</td>
        <td>spring-framework</td>
        <td>开发状态</td>
    </tr>
    <tr>
        <td>4.3.x</td>
        <td>4.3.x</td>
        <td>开发中</td>
    </tr>
    <tr>
        <td>5.0.x</td>
        <td>5.0.x</td>
        <td>开发中</td>
    </tr>
    <tr>
        <td>5.1.x</td>
        <td>5.1.x</td>
        <td>开发中</td>
    </tr>
</table>

## 核心配置
```xml
<dependency>
     <groupId>org.xbdframework</groupId>
     <artifactId>xbd-context-support</artifactId>
     <version>版本号</version>
</dependency>
```


## xbd-core

```xml
<dependency>
    <groupId>org.xbdframework</groupId>
    <artifactId>xbd-core</artifactId>
    <version>版本号</version>
</dependency>
```

## xbd-context

```xml
<dependency>
    <groupId>org.xbdframework</groupId>
    <artifactId>xbd-context</artifactId>
    <version>版本号</version>
</dependency>
```

## xbd-context-support

```xml
<dependency>
    <groupId>org.xbdframework</groupId>
    <artifactId>xbd-context-support</artifactId>
    <version>版本号</version>
</dependency>
```

## xbd-dao

```xml
<dependency>
    <groupId>org.xbdframework</groupId>
    <artifactId>xbd-dao</artifactId>
    <version>版本号</version>
</dependency>
```

## xbd-tx

```xml
<dependency>
    <groupId>org.xbdframework</groupId>
    <artifactId>xbd-tx</artifactId>
    <version>版本号</version>
</dependency>
```

## xbd-web

```xml
<dependency>
    <groupId>org.xbdframework</groupId>
    <artifactId>xbd-web</artifactId>
    <version>版本号</version>
</dependency>
```

## xbd-jms

```xml
<dependency>
    <groupId>org.xbdframework</groupId>
    <artifactId>xbd-jms</artifactId>
    <version>版本号</version>
</dependency>
```

## xbd-framework-bom

```xml
<dependency>
    <groupId>org.xbdframework</groupId>
    <artifactId>xbd-framework-bom</artifactId>
    <version>版本号</version>
</dependency>
```


