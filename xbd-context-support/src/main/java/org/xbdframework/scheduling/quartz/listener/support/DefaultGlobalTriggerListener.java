package org.xbdframework.scheduling.quartz.listener.support;

import java.time.LocalDateTime;

import org.xbdframework.scheduling.quartz.listener.AbstractTriggerListener;

import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.Trigger.CompletedExecutionInstruction;

/**
 * 默认全局TriggerListener
 *
 * @author luas
 * @since 4.3
 */
public class DefaultGlobalTriggerListener extends AbstractTriggerListener {

	@Override
	public String getName() {
		return "defaultGlobalTriggerListener";
	}

	@Override
	public void triggerFired(Trigger trigger, JobExecutionContext context) {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Trigger {} 于 {}执行，上次执行 {}，下次执行 {}",
					new Object[] { trigger.getKey().toString(), LocalDateTime.now(),
							trigger.getPreviousFireTime(), trigger.getNextFireTime() });
		}
	}

	@Override
	public boolean vetoJobExecution(Trigger trigger, JobExecutionContext context) {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Trigger {} 于 {} 执行否决，否决结果 {}", new Object[] {
					trigger.getKey().toString(), LocalDateTime.now(), false });
		}

		return false;
	}

	@Override
	public void triggerMisfired(Trigger trigger) {
		if (this.logger.isDebugEnabled()) {
			logger.debug("Trigger {} 未执行，上次执行 {}，下次执行 {}",
					new Object[] { trigger.getKey().toString(),
							trigger.getPreviousFireTime(), trigger.getNextFireTime() });
		}
	}

	@Override
	public void triggerComplete(Trigger trigger, JobExecutionContext context,
			CompletedExecutionInstruction triggerInstructionCode) {
		if (this.logger.isDebugEnabled()) {
			logger.debug("Trigger {} 于 {} 执行完成，上次执行：{}，下次执行：{}",
					new Object[] { trigger.getKey().toString(), LocalDateTime.now(),
							trigger.getPreviousFireTime(), trigger.getNextFireTime() });
		}
	}

}
