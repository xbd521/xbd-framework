package org.xbdframework.scheduling.quartz;

/**
 * 定时任务基类
 * <p>
 * 声明新的定时任务时，需继承此类，如不使用默认方法，需注意设置{@link XbdQuartzJobBean#setTargetMethod(String)}
 * </p>
 *
 * @author luas
 * @since 4.3
 */
public abstract class AbstractQuartzTask {

	public final static String DEFAULT_TARGETMETHOD = "execute";

	public abstract void execute() throws Exception;

}
