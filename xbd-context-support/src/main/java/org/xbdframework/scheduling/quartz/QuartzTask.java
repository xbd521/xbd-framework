package org.xbdframework.scheduling.quartz;

import java.io.Serializable;
import java.util.Date;

import org.quartz.CronTrigger;

/**
 * 定时任务实体，包含任务名称、任务分组、任务描述、任务开始时间、是否立即开始、
 * 任务结束时间、任务目标类、任务目标类SpringBean对象、任务目标类执行方法、cron表达式等属性
 *
 * @author luas
 * @since 4.3
 */
public class QuartzTask implements Serializable {

	private static final long serialVersionUID = -2116518270025568628L;

	private String name;

	private String group;

	private String description;

	private Date startAt;

	private boolean startNow;

	private Date endAt;

	private String targetClass;

	private String targetObject;

	private String targetMethod;

	private String targetMethodParam;

	private String cronExpression;

	/**
	 * 任务错过触发时间执行策略 <pre>
	 *      MISFIRE_INSTRUCTION_SMART_POLICY Quartz框架自动选择适合的策略
	 *      MISFIRE_INSTRUCTION_DO_NOTHING 不触发立即执行，等待下次Cron触发频率到达时刻开始按照Cron频率依次执行
	 *      MISFIRE_INSTRUCTION_FIRE_ONCE_NOW 以当前时间为触发频率立刻触发一次执行，然后按照Cron频率依次执行
	 *      MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY 以错过的第一个频率时间立刻开始执行，重做错过的所有频率周期后，当下一次触发频率发生时间大于当前时间后，再按照正常的Cron频率依次执行
	 * </pre>
	 *
	 * @see org.quartz.CronTrigger#MISFIRE_INSTRUCTION_SMART_POLICY
	 * @see org.quartz.CronTrigger#MISFIRE_INSTRUCTION_DO_NOTHING
	 * @see org.quartz.CronTrigger#MISFIRE_INSTRUCTION_FIRE_ONCE_NOW
	 * @see org.quartz.CronTrigger#MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY
	 */
	private int misfireInstruction = CronTrigger.MISFIRE_INSTRUCTION_SMART_POLICY;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartAt() {
		return startAt;
	}

	public void setStartAt(Date startAt) {
		this.startAt = startAt;
	}

	public boolean isStartNow() {
		return startNow;
	}

	public void setStartNow(boolean startNow) {
		this.startNow = startNow;
	}

	public Date getEndAt() {
		return endAt;
	}

	public void setEndAt(Date endAt) {
		this.endAt = endAt;
	}

	public String getTargetClass() {
		return targetClass;
	}

	public void setTargetClass(String targetClass) {
		this.targetClass = targetClass;
	}

	public String getTargetObject() {
		return targetObject;
	}

	public void setTargetObject(String targetObject) {
		this.targetObject = targetObject;
	}

	public String getTargetMethod() {
		return targetMethod;
	}

	public void setTargetMethod(String targetMethod) {
		this.targetMethod = targetMethod;
	}

	public String getTargetMethodParam() {
		return targetMethodParam;
	}

	public void setTargetMethodParam(String targetMethodParam) {
		this.targetMethodParam = targetMethodParam;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public int getMisfireInstruction() {
		return misfireInstruction;
	}

	public void setMisfireInstruction(int misfireInstruction) {
		this.misfireInstruction = misfireInstruction;
	}

}
