package org.xbdframework.scheduling.quartz;

import org.quartz.spi.TriggerFiredBundle;

import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

/**
 * 自动装载Bean到Spring，可在JobBean中直接注入定义的Bean
 *
 * @author luas
 * @since 4.3
 */
public class AutowireCapableXbdBeanJobFactory extends SpringBeanJobFactory {

	private final ApplicationContext applicationContext;

	public AutowireCapableXbdBeanJobFactory(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	protected Object createJobInstance(TriggerFiredBundle bundle) throws Exception {
		Object jobInstance = super.createJobInstance(bundle);
		this.applicationContext.getAutowireCapableBeanFactory().autowireBean(jobInstance);
		this.applicationContext.getAutowireCapableBeanFactory()
				.initializeBean(jobInstance, null);
		return jobInstance;
	}

}
