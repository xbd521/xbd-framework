package org.xbdframework.trace.annotation;

import java.lang.annotation.*;

import org.xbdframework.trace.TraceHandler;

/**
 * Describes trace attributes on a method or class.
 * 描述方法或类的追踪属性.
 *
 * <p>This annotation type is generally directly comparable to xbd's
 * {@link org.xbdframework.trace.interceptor.DefaultTraceAttribute} class, and in
 * fact {@link AnnotationTraceAttributeSource} will directly convert the data to the
 * latter class, so that xbd's trace support code does not have to know about annotations.
 *
 * <p>此注解与类{@link org.xbdframework.trace.interceptor.DefaultTraceAttribute}
 * 定义内容几乎一致.实际上，{@link AnnotationTraceAttributeSource}会将数据转换为后者.
 * 因此，追踪框架对使用者来说是透明的，无需知晓注解内容.
 *
 * <p>For specific information about the semantics of this annotation's attributes,
 * consult the {@link org.xbdframework.trace.TraceDefinition} and
 * {@link org.xbdframework.trace.interceptor.TraceAttribute} javadocs.
 *
 * <p>有关此注解的其它信息，请参阅{@link org.xbdframework.trace.TraceDefinition}和
 * {@link org.xbdframework.trace.interceptor.TraceAttribute}的javadocs.
 *
 * @author luas
 * @since 4.3
 * @see org.xbdframework.trace.interceptor.TraceAttribute
 * @see org.xbdframework.trace.interceptor.DefaultTraceAttribute
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Traceable {

    /**
     * determine whether the current tracing is enabled.
     * @return the current tracing is enabled.
     */
    boolean enabled() default true;

    /**
     * determine whether the current tracing context's logger is enabled.
     * @return the current tracing context's logger is enabled.
     */
    boolean loggerEnabled() default true;

    /**
     * the current tracing context's logger name.
     * @return the current tracing context's logger name.
     */
    String loggerName() default "";

    /**
     * claiming that during tracing, the traced information's handlers.
     * @return {@link org.xbdframework.trace.TraceHandler} bean names to be invoked.
     */
    String[] handlerRefs() default {};

    /**
     * claiming that during tracing, the traced information's handlers.
     * @return {@link org.xbdframework.trace.TraceHandler} classes to be invoked.
     */
    Class<? extends TraceHandler>[] handlers() default {};

    /**
     * determine whether to record exception stack trace information when exception occurs during tracing.
     * @return whether to record exception stack trace information.
     */
    boolean printStackTrace() default true;

}
