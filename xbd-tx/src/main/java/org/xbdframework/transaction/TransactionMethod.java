package org.xbdframework.transaction;

import java.util.HashMap;
import java.util.Map;

public enum TransactionMethod {

	SAVE("save*"),

	DELETE("delete*"),

	UPDATE("update*"),

	CREATE("create*"),

	EXEC("exec*"),

	GET("get*"),

	SELECT("select*"),

	QUERY("query*"),

	FIND("find*"),

	LIST("list*"),

	COUNT("count*"),

	IS("is*");

	private String value;

	TransactionMethod(String value) {
		this.value = value;
	}

	private static final Map<String, TransactionMethod> mappings = new HashMap<>(15);

	static {
		for (TransactionMethod httpMethod : values()) {
			mappings.put(httpMethod.value, httpMethod);
		}
	}

	public static boolean contains(String value) {
		return mappings.keySet().parallelStream()
				.anyMatch(method -> contains(method, value));
	}

	public static boolean contains(String originalValue, String value) {
		String valueRaw = value.endsWith("*") ? value.substring(0, value.length() - 1)
				: value;
		String originalValueRaw = originalValue.endsWith("*")
				? originalValue.substring(0, originalValue.length() - 1) : originalValue;

		return valueRaw.equals(originalValueRaw) || valueRaw.startsWith(originalValueRaw);
	}

	public String getValue() {
		return value;
	}

}
