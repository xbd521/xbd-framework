package org.xbdframework.transaction;

import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Test;

public class TransactionMethodTest {

    @Test
    public void value() {
        Stream.of(TransactionMethod.values()).forEach(n -> System.out.println(n.getValue()));
    }

    @Test
    public void containsNoXinghao() {
        Assert.assertTrue(TransactionMethod.contains("count"));
    }

    @Test
    public void containsXinghaoAndSameWithOriginValue() {
        Assert.assertTrue(TransactionMethod.contains("count*"));
    }

    @Test
    public void containsXinghaoAndStartWith() {
        Assert.assertTrue(TransactionMethod.contains("countById*"));
    }

    @Test
    public void containsXinghaoAndNotStartWith() {
        Assert.assertFalse(TransactionMethod.contains("coun*"));
    }

}