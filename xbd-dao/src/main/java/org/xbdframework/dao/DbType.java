package org.xbdframework.dao;

/**
 * 数据库类型，目前仅支持oracle、mysql数据库，随后会支持更多数据库
 *
 * 支持的数据库如下： <pre>
 *      ORACLE
 *      MYSQL
 * </pre>
 *
 * @author luas
 * @since 4.3
 */
public enum DbType {

	ORACLE("oracle"), MYSQL("mysql");

	private String type;

	DbType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
