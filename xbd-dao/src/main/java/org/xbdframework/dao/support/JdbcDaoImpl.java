package org.xbdframework.dao.support;

import java.math.BigDecimal;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.util.Assert;

import org.xbdframework.dao.BaseDao;
import org.xbdframework.dao.PageHelper;
import org.xbdframework.dao.DbType;
import org.xbdframework.dao.core.BasePo;
import org.xbdframework.dao.core.PageInfo;
import org.xbdframework.dao.core.support.XbdJdbcDaoSupport;
import org.xbdframework.dao.support.pagehelper.JdbcPageHelper;
import org.xbdframework.jdbc.core.SqlAndParam;

/**
 * 持久化、对象获取、单值查询、列表查询等SpringJdbc实现{@link BaseDao}
 *
 * <p>
 * 主要使用SpringJdbc中的{@link JdbcTemplate}、{@link NamedParameterJdbcTemplate}。
 * 针对分页，目前已同步实现Jdbc方式的分页{@link JdbcPageHelper}。 也可实现别的方式的分页，具体参考分页接口{@link PageHelper}。
 * </p>
 *
 * <pre class="code">
 * &#064;Bean
 * public PageHelper mysqlPageHelper() {
 *     return new MysqlPageHelper(DbType.MYSQL);
 * }
 *
 * &#064;Bean
 * public JdbcDaoImpl dao() {
 *     JdbcDaoImpl jdbcDaoImpl = new JdbcDaoImpl();
 *
 *     jdbcDaoImpl.setJdbcTemplate(jdbcTemplate);
 *     jdbcDaoImpl.setNamedParameterJdbcTemplate(namedParameterJdbcTemplate);
 *     jdbcDaoImpl.setPageHelper(jdbcPageHelper);
 *
 *     return jdbcDaoImpl;
 * }
 *
 * {@code
 *   <bean id="mysqlPageHelper" class="org.xbdframework.dao.support.pagehelper.MysqlPageHelper">
 *       <constructor-arg index="0" value="MYSQL" />
 *   </bean>
 *
 *   <bean id="dao" class="org.xbdframework.dao.support.JdbcDaoImpl">
 *       <property name="jdbcTemplate" ref="jdbcTemplate" />
 *       <property name="namedParameterJdbcTemplate" ref="namedParameterJdbcTemplate" />
 *       <property name="pageHelper" ref="jdbcPageHelper" />
 *   </bean>
 * }
 * </pre>
 *
 * @author luas
 * @since 4.3
 * @see JdbcTemplate
 * @see NamedParameterJdbcTemplate
 * @see JdbcPageHelper
 * @see org.xbdframework.dao.support.pagehelper.MysqlPageHelper
 * @see org.xbdframework.dao.support.pagehelper.OraclePageHelper
 * @see PageHelper
 * @see DbType
 */
@SuppressWarnings({ "unchecked", "rawtype" })
public class JdbcDaoImpl extends XbdJdbcDaoSupport implements BaseDao {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 插入
	 * <p>
	 * 具体插入的值为Po中已set的字段
	 * </p>
	 * @param basePo PO对象
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 插入成功的行数，如插入失败，则返回{@code 0}；如插入成功，则返回{@code 1}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> int insert(T basePo) throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空！");
		Assert.notNull(basePo.getPkValue(), basePo.getClass().getCanonicalName() + "主键"
				+ basePo.getPkName() + "不能为空");

		SqlAndParam<Map<String, Object>> sp = basePo.getInsertSql();

		return update(sp.getSql(), sp.getParam());
	}

	/**
	 * 插入
	 * <p>
	 * 具体插入的值为Po中已set的字段
	 * </p>
	 * @param basePos PO对象集合
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 插入成功的行数，全部插入失败，返回{@code 0}；
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> int insert(List<T> basePos) throws DataAccessException {
		Assert.notNull(basePos, "basePos不能为空！");

		return basePos.stream().parallel().mapToInt(basePo -> insert(basePo)).reduce(0,
				Integer::sum);
	}

	/**
	 * 按主键更新
	 * <p>
	 * 具体更新的值为Po中已set的字段
	 * </p>
	 * @param basePo PO对象
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 更新成功的行数，如更新失败，则返回{@code 0}；如更新成功，则返回{@code 1}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> int update(T basePo) throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空！");
		Assert.notNull(basePo.getPkValue(), basePo.getClass().getCanonicalName() + "主键"
				+ basePo.getPkName() + "不能为空");

		SqlAndParam<Map<String, Object>> sp = basePo.getUpdateSql();

		return update(sp.getSql(), sp.getParam());
	}

	/**
	 * 按主键更新
	 * <p>
	 * 具体更新的值为Po中已set的字段
	 * </p>
	 * @param basePos PO对象集合
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 更新成功的行数，全部更新失败，返回{@code 0}；
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> int update(List<T> basePos) throws DataAccessException {
		Assert.notNull(basePos, "basePos不能为空！");

		return basePos.stream().parallel().mapToInt(basePo -> update(basePo)).reduce(0,
				Integer::sum);
	}

	/**
	 * 按条件更新
	 * <p>
	 * 需要更新的字段可通过Po进行设置，更新范围通过{@code where}参数进行设置
	 * </p>
	 * <pre>
	 *     XxPo xxPo = new XxPo();
	 *     xxPo.setStatus(2);
	 *     xxPo.setAddUser("张三");
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     update(xxPo, "name = ?", params);
	 *     update(xxPo, "name = ?", "王五");
	 *     update(xxPo, "name = ?", new Object[] { "王五" });
	 *
	 *     生成的sql语句为：update xx set status = 2, adduser = '张三' where name = '王五'
	 * </pre>
	 * @param basePo PO对象集合
	 * @param where sql {@code where}条件，形如{@code " status = :status"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 更新成功的行数。满足条件的行都会被更新，返回其中更新成功的行数。
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> int update(T basePo, String where, Object... params)
			throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空!");
		Assert.notNull(where, "where不能为空!");
		Assert.notNull(params, "params不能为空!");

		SqlAndParam<Object[]> sp = basePo.getUpdateSql(where, params);

		return update(sp.getSql(), sp.getParam());
	}

	/**
	 * 按条件更新
	 * <p>
	 * 需要更新的字段可通过Po进行设置，更新范围通过{@code where}参数进行指定
	 * </p>
	 * <pre>
	 *     XxPo xxPo = new XxPo();
	 *     xxPo.setStatus(2);
	 *     xxPo.setAddUser("张三");
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     update(xxPo, "name = :name", params);
	 *
	 *     生成的sql语句为：update xx set status = 2, adduser = '张三' where name = '王五'
	 * </pre>
	 * @param basePo PO对象集合
	 * @param where sql {@code where}条件，形如{@code " status = :status"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 更新成功的行数。满足条件的行都会被更新，返回其中更新成功的行数。
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> int update(T basePo, String where,
			Map<String, Object> params) throws DataAccessException {
		return update(basePo, where, new MapSqlParameterSource(params));
	}

	/**
	 * 按条件更新
	 * <p>
	 * 需要更新的字段可通过Po进行设置，更新范围通过{@code where}参数进行指定
	 * </p>
	 * <pre>
	 *     XxPo xxPo = new XxPo();
	 *     xxPo.setStatus(2);
	 *     xxPo.setAddUser("张三");
	 *
	 *    {@code MapSqlParameterSource params = new MapSqlParameterSource();}
	 *     params.addValue("name", "王五");
	 *
	 *     update(xxPo, "name = :name", params);
	 *
	 *     生成的sql语句为：update xx set status = 2, adduser = '张三' where name = '王五'
	 * </pre>
	 * @param basePo PO对象集合
	 * @param where sql {@code where}条件，形如{@code " status = :status"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 更新成功的行数。满足条件的行都会被更新，返回其中更新成功的行数。
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> int update(T basePo, String where,
			SqlParameterSource params) throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空!");
		Assert.notNull(where, "where不能为空!");
		Assert.notNull(params, "params不能为空!");

		SqlAndParam<SqlParameterSource> sp = basePo.getUpdateSql(where, params);

		return update(sp.getSql(), sp.getParam());
	}

	/**
	 * 按主键删除
	 * @param basePo PO对象
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 删除成功的行数，如删除失败，则返回{@code 0}；如删除成功，则返回{@code 1}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> int delete(T basePo) throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空！");
		Assert.notNull(basePo.getPkValue(), basePo.getClass().getCanonicalName() + " 主键"
				+ basePo.getPkName() + "不能为空");

		SqlAndParam<Map<String, Object>> sp = basePo.getDeleteSql();

		return update(sp.getSql(), sp.getParam());
	}

	/**
	 * 按主键删除
	 * @param basePos PO对象集合
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 删除成功的行数，全部删除失败，返回{@code 0}；
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> int delete(List<T> basePos) throws DataAccessException {
		Assert.notNull(basePos, "basePos不能为空！");

		return basePos.stream().parallel().mapToInt(basePo -> delete(basePo)).reduce(0,
				Integer::sum);
	}

	/**
	 * 按条件删除
	 * <p>
	 * 删除范围通过{@code where}参数进行指定
	 * </p>
	 * <pre>
	 *     XxPo xxPo = new XxPo();
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     update(xxPo, "name = ?", params);
	 *     update(new XxPo(), "name = ?", "王五");
	 *     update(new XxPo(), "name = ?", new Object[] { "王五" });
	 *
	 *     生成的sql语句为：delete from xx where name = '王五'
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :status"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 删除成功的行数。满足条件的行都会被删除，返回其中删除成功的行数。
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> int delete(T basePo, String where, Object... params)
			throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空！");
		Assert.notNull(where, "where不能为空!");
		Assert.notNull(params, "params不能为空!");

		SqlAndParam<Object[]> sp = basePo.getDeleteSql(where, params);

		return update(sp.getSql(), sp.getParam());
	}

	/**
	 * 按条件删除
	 * <p>
	 * 删除范围通过{@code where}参数进行指定
	 * </p>
	 * <pre>
	 *     XxPo xxPo = new XxPo();
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     update(xxPo, "name = ?", params);
	 *
	 *     生成的sql语句为：delete from xx where name = '王五'
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :status"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 删除成功的行数。满足条件的行都会被删除，返回其中删除成功的行数。
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> int delete(T basePo, String where,
			Map<String, Object> params) throws DataAccessException {
		return update(basePo, where, new MapSqlParameterSource(params));
	}

	/**
	 * 按条件删除
	 * <p>
	 * 删除范围通过{@code where}参数进行指定
	 * </p>
	 * <pre>
	 *     XxPo xxPo = new XxPo();
	 *
	 *    {@code MapSqlParameterSource params = new MapSqlParameterSource();}
	 *     params.addValue("name", "王五");
	 *
	 *     update(xxPo, "name = ?", params);
	 *
	 *     生成的sql语句为：delete from xx where name = '王五'
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :status"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 删除成功的行数。满足条件的行都会被删除，返回其中删除成功的行数。
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> int delete(T basePo, String where,
			SqlParameterSource params) throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空！");
		Assert.notNull(where, "where不能为空!");
		Assert.notNull(params, "params不能为空!");

		SqlAndParam<SqlParameterSource> sp = basePo.getDeleteSql(where, params);

		return update(sp.getSql(), sp.getParam());
	}

	/**
	 * 按主键查询单PO
	 *
	 * <pre>
	 *     XxPo xxPo = get(new XxPo("1122"));
	 * </pre>
	 * @param basePo PO对象
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 如该主键对应的有记录，则返回该行记录的数据库表对象；如没有，则返回{@code null}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> T get(T basePo) throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空！");
		Assert.notNull(basePo.getPkValue(), basePo.getClass().getCanonicalName() + "主键"
				+ basePo.getPkName() + "不能为空");

		SqlAndParam<Map<String, Object>> sp = basePo.getSelectSql();

		return (T) get(sp.getSql(), sp.getParam(),
				new BeanPropertyRowMapper<>(basePo.getClass()));
	}

	/**
	 * 按条件查询PO
	 * <p>
	 * 查询范围通过{@code where}进行指定
	 * </p>
	 *
	 * <pre>
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     XxPo xxPo = get(new XxPo(), "name = ?", params);
	 *     XxPo xxPo = get(new XxPo(), "name = ?", "王五");
	 *     XxPo xxPo = get(new XxPo(), "name = ?", new Object[] { "王五" });
	 * </pre>
	 * @param basePo PO对象
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 如where条件对应的有记录，则返回该行记录的数据库表对象；如没有，则返回{@code null}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> T get(T basePo, String where, Object... params)
			throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空！");
		Assert.notNull(where, "where不能为空!");
		Assert.notNull(params, "params不能为空!");

		SqlAndParam<Object[]> sp = basePo.getSelectSql(where, params);

		return (T) get(sp.getSql(), sp.getParam(),
				new BeanPropertyRowMapper<>(basePo.getClass()));
	}

	/**
	 * 按条件查询单PO
	 * <p>
	 * 查询范围通过{@code where}进行指定
	 * </p>
	 *
	 * <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     XxPo xxPo = get(new XxPo(), "name = ?", params);
	 * </pre>
	 * @param basePo PO对象
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 如where条件对应的有记录，则返回该行记录的数据库表对象；如没有，则返回{@code null}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> T get(T basePo, String where, Map<String, Object> params)
			throws DataAccessException {
		return (T) get(basePo, where, new MapSqlParameterSource(params));
	}

	/**
	 * 按条件查询单PO
	 * <p>
	 * 查询范围通过{@code where}进行指定
	 * </p>
	 *
	 * <pre>
	 *    {@code MapSqlParameterSource params = new MapSqlParameterSource();}
	 *     params.addValue("name", "王五");
	 *
	 *     XxPo xxPo = get(new XxPo(), "name = ?", params);
	 * </pre>
	 * @param basePo PO对象
	 * @param where {@code where}条件，如status = :status
	 * @param params {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 如where条件对应的有记录，则返回该行记录的数据库表对象；如没有，则返回{@code null}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public <T extends BasePo> T get(T basePo, String where, SqlParameterSource params)
			throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空！");
		Assert.notNull(where, "where不能为空!");
		Assert.notNull(params, "params不能为空!");

		SqlAndParam<SqlParameterSource> sp = basePo.getSelectSql(where, params);

		return (T) get(sp.getSql(), sp.getParam(),
				new BeanPropertyRowMapper<>(basePo.getClass()));
	}

	/**
	 * 查询Map
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select * from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code Map<String, Object> xxMap = get(sql, params)}
	 *    {@code Map<String, Object> xxMap = get(sql, "王五")}
	 *    {@code Map<String, Object> xxMap = get(sql, new Object[] { "王五" })}
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public Map<String, Object> get(String sql, Object... params)
			throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");

		return get(sql, params, new ColumnMapRowMapper());
	}

	/**
	 * 查询Map
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select * from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *    {@code Map<String, Object> xxMap = get(sql, params)}
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public Map<String, Object> get(String sql, Map<String, Object> params)
			throws DataAccessException {
		return get(sql, new MapSqlParameterSource(params));
	}

	/**
	 * 查询Map
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select * from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *    {@code Map<String, Object> xxMap = get(sql, params)}
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public Map<String, Object> get(String sql, SqlParameterSource params)
			throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");

		return get(sql, params, new ColumnMapRowMapper());
	}

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     String xx = get(sql, String.class, params);
	 *     String xx = get(sql, String.class, "王五")
	 *     String xx = get(sql, String.class, new Object[] { "王五" })
	 *     Long xx = get(sql, params, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param requiredType 指定值类型的Class
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public <T> T get(String sql, Class<T> requiredType, Object... params)
			throws DataAccessException {
		return get(sql, params, requiredType);
	}

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     String xx = get(sql, params, String.class);
	 *     String xx = get(sql, new Object[] { "王五" }, String.class)
	 *     Long xx = get(sql, params, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public <T> T get(String sql, Object[] params, Class<T> requiredType)
			throws DataAccessException {
		return queryForObject(sql, params, requiredType);
	}

	/**
	 * 查询class对象
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     String xx = get(sql, params, String.class);
	 *     Long xx = get(sql, params, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public <T> T get(String sql, Map<String, Object> params, Class<T> requiredType)
			throws DataAccessException {
		return get(sql, new MapSqlParameterSource(params), requiredType);
	}

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     String xx = get(sql, params, String.class);
	 *     Long xx = get(sql, params, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public <T> T get(String sql, SqlParameterSource params, Class<T> requiredType)
			throws DataAccessException {
		return queryForObject(sql, params, requiredType);
	}

	/**
	 * 查询指定类型的对象
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code RowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code String xx = get(sql, new SingleColumnRowMapper<>(String.class), params)};
	 *    {@code String xx = get(sql, new SingleColumnRowMapper<>(String.class), "王五")};
	 *    {@code String xx = get(sql, new SingleColumnRowMapper<>(String.class), new Object[] { "王五" })};
	 *    {@code Long xx = get(sql, new SingleColumnRowMapper<>(Long.class), "王五")};
	 *    {@code Long xx = get(sql, new SingleColumnRowMapper<>(Long.class), params)};
	 *    {@code XxPo xxPo = get(sql, new BeanPropertyRowMapper<>(XxPo.class), "王五"};
	 *    {@code XxPo xxPo = get(sql, new BeanPropertyRowMapper<>(XxPo.class), params};
	 *    {@code Map<String, Object> xxMap = get(sql, new ColumnMapRowMapper(), "王五"};
	 *    {@code Map<String, Object> xxMap = get(sql, new ColumnMapRowMapper(), params};
	 * </pre>
	 * @param sql sql语句
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	public <T> T get(String sql, RowMapper<T> rowMapper, Object... params)
			throws DataAccessException {
		return get(sql, params, rowMapper);
	}

	/**
	 * 查询指定类型的对象
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code RowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code String xx = get(sql, params, new SingleColumnRowMapper<>(String.class))};
	 *    {@code String xx = get(sql, new Object[] { "王五" }, new SingleColumnRowMapper<>(String.class))};
	 *    {@code Long xx = get(sql, params, new SingleColumnRowMapper<>(Long.class))};
	 *    {@code XxPo xxPo = get(sql, params, new BeanPropertyRowMapper<>(XxPo.class)};
	 *    {@code Map<String, Object> xxMap = get(sql, params, new ColumnMapRowMapper()};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see SingleColumnRowMapper
	 * @see BeanPropertyRowMapper
	 * @see ColumnMapRowMapper
	 */
	public <T> T get(String sql, Object[] params, RowMapper<T> rowMapper)
			throws DataAccessException {
		Assert.notNull(sql, "sql不能为空！");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(rowMapper, "rowMapper不能为空!");

		return queryForObject(sql, params, rowMapper);
	}

	/**
	 * 查询指定类型的对象
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code RowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code String xx = get(sql, params, new SingleColumnRowMapper<>(String.class))};
	 *    {@code String xx = get(sql, new Object[] { "王五" }, new SingleColumnRowMapper<>(String.class))};
	 *    {@code Long xx = get(sql, params, new SingleColumnRowMapper<>(Long.class))};
	 *    {@code XxPo xxPo = get(sql, params, new BeanPropertyRowMapper<>(XxPo.class)};
	 *    {@code Map<String, Object> xxMap = get(sql, params, new ColumnMapRowMapper()};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see SingleColumnRowMapper
	 * @see BeanPropertyRowMapper
	 * @see ColumnMapRowMapper
	 */
	public <T> T get(String sql, Map<String, Object> params, RowMapper<T> rowMapper)
			throws DataAccessException {
		return get(sql, new MapSqlParameterSource(params), rowMapper);
	}

	/**
	 * 查询指定类型的对象
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code RowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *    {@code String xx = get(sql, params, new SingleColumnRowMapper<>(String.class))};
	 *    {@code Long xx = get(sql, params, new SingleColumnRowMapper<>(Long.class))};
	 *    {@code XxPo xxPo = get(sql, params, new BeanPropertyRowMapper<>(XxPo.class)};
	 *    {@code Map<String, Object> xxMap = get(sql, params, new ColumnMapRowMapper()};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see SingleColumnRowMapper
	 * @see BeanPropertyRowMapper
	 * @see ColumnMapRowMapper
	 */
	public <T> T get(String sql, SqlParameterSource params, RowMapper<T> rowMapper)
			throws DataAccessException {
		Assert.notNull(sql, "sql不能为空！");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(rowMapper, "rowMapper不能为空!");

		return queryForObject(sql, params, rowMapper);
	}

	/**
	 * 按条件查询PO列表 <pre>
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", params)};
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", "王五")};
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", new Object[] { "王五" })};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 */
	public <T extends BasePo> List<T> select(T basePo, String where, Object... params)
			throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空!");
		Assert.notNull(params, "params不能为空!");

		SqlAndParam<Object[]> sp = basePo.getSelectSql(where, params);

		return (List<T>) select(sp.getSql(), sp.getParam(),
				new BeanPropertyRowMapper<>(basePo.getClass()));
	}

	/**
	 * 按条件查询PO列表 <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = :name", params)};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 */
	public <T extends BasePo> List<T> select(T basePo, String where,
			Map<String, Object> params) throws DataAccessException {
		return select(basePo, where, new MapSqlParameterSource(params));
	}

	/**
	 * 按条件查询PO列表 <pre>
	 *    {@code MapSqlParameterSource params = new MapSqlParameterSource();}
	 *     params.addValue("name", "王五");
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = :name", params)};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 */
	public <T extends BasePo> List<T> select(T basePo, String where,
			SqlParameterSource params) throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空!");
		Assert.notNull(params, "params不能为空!");

		SqlAndParam<SqlParameterSource> sp = basePo.getSelectSql(where, params);

		return (List<T>) select(sp.getSql(), sp.getParam(),
				new BeanPropertyRowMapper<>(basePo.getClass()));
	}

	/**
	 * 按条件分页查询PO列表 <pre>
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", params, pageInfo)};
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", new Object[] { "王五" }, pageInfo)};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param pageInfo 分页信息
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足where及分页条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 * @see PageInfo
	 */
	public <T extends BasePo> List<T> select(T basePo, String where, PageInfo pageInfo,
			Object... params) throws DataAccessException {
		return select(basePo, where, params, pageInfo);
	}

	/**
	 * 按条件分页查询PO列表 <pre>
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", params, pageInfo)};
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", new Object[] { "王五" }, pageInfo)};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足where及分页条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 * @see PageInfo
	 */
	public <T extends BasePo> List<T> select(T basePo, String where, Object[] params,
			PageInfo pageInfo) throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(pageInfo, "pageInfo不能为空!");

		SqlAndParam<Object[]> sp = basePo.getSelectSql(where, params);

		return (List<T>) select(sp.getSql(), sp.getParam(), pageInfo,
				new BeanPropertyRowMapper<>(basePo.getClass()));
	}

	/**
	 * 按条件分页查询PO列表 <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = :name", params, pageInfo)};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足where及分页条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 * @see PageInfo
	 */
	public <T extends BasePo> List<T> select(T basePo, String where,
			Map<String, Object> params, PageInfo pageInfo) throws DataAccessException {
		return select(basePo, where, new MapSqlParameterSource(params));
	}

	/**
	 * 按条件分页查询PO列表 <pre>
	 *    {@code MapSqlParameterSource params = new MapSqlParameterSource();}
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = :name", params, pageInfo)};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足where及分页条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 * @see PageInfo
	 */
	public <T extends BasePo> List<T> select(T basePo, String where,
			SqlParameterSource params, PageInfo pageInfo) throws DataAccessException {
		Assert.notNull(basePo, "basePo不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(pageInfo, "pageInfo不能为空!");

		SqlAndParam<SqlParameterSource> sp = basePo.getSelectSql(where, params);

		return (List<T>) select(sp.getSql(), sp.getParam(),
				new BeanPropertyRowMapper<>(basePo.getClass()));
	}

	/**
	 * 查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code List<String> xxs = select(sql, String.class, params)};
	 *    {@code List<String> xxs = select(sql, String.class, "王五")};
	 *    {@code List<String> xxs = select(sql, String.class, new Object[] { "王五" })};
	 *    {@code List<Long> xxs = select(sql, params, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param requiredType 指定值类型的Class
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public <T> List<T> select(String sql, Class<T> requiredType, Object... params)
			throws DataAccessException {
		return select(sql, params, requiredType);
	}

	/**
	 * 查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code List<String> xxs = select(sql, params, String.class)};
	 *    {@code List<String> xxs = select(sql, new Object[] { "王五" }, String.class)};
	 *    {@code List<Long> xxs = select(sql, params, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public <T> List<T> select(String sql, Object[] params, Class<T> requiredType)
			throws DataAccessException {
		return select(sql, params, SingleColumnRowMapper.newInstance(requiredType));
	}

	/**
	 * 查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *    {@code List<String> xxs = select(sql, params, String.class)};
	 *    {@code List<Long> xxs = select(sql, params, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public <T> List<T> select(String sql, Map<String, Object> params,
			Class<T> requiredType) throws DataAccessException {
		return select(sql, params, SingleColumnRowMapper.newInstance(requiredType));
	}

	/**
	 * 查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *    {@code List<String> xxs = select(sql, params, String.class)};
	 *    {@code List<Long> xxs = select(sql, params, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public <T> List<T> select(String sql, SqlParameterSource params,
			Class<T> requiredType) throws DataAccessException {
		return select(sql, params, SingleColumnRowMapper.newInstance(requiredType));
	}

	/**
	 * 分页查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<String> xxs = select(sql, pageInfo, String.class, params)};
	 *    {@code List<String> xxs = select(sql, pageInfo, String.class, "王五")};
	 *    {@code List<String> xxs = select(sql, pageInfo, String.class, new Object[] { "王五" })};
	 *    {@code List<Long> xxs = select(sql, pageInfo, Long.class, "王五")};
	 *    {@code List<Long> xxs = select(sql, pageInfo, Long.class, params)};
	 * </pre>
	 * @param sql sql语句
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	public <T> List<T> select(String sql, PageInfo pageInfo, Class<T> requiredType,
			Object... params) throws DataAccessException {
		return select(sql, params, pageInfo, requiredType);
	}

	/**
	 * 分页查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<String> xxs = select(sql, params, pageInfo, String.class)};
	 *    {@code List<String> xxs = select(sql, new Object[] { "王五" }, pageInfo, String.class)};
	 *    {@code List<Long> xxs = select(sql, params, pageInfo, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	public <T> List<T> select(String sql, Object[] params, PageInfo pageInfo,
			Class<T> requiredType) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(pageInfo, "pageInfo不能为空!");
		Assert.notNull(requiredType, "requiredType!");

		int cnt = queryForInt("select count(1) from (" + sql + ") total", params);

		pageInfo.setTotal(cnt);

		SqlAndParam<Object[]> sp = getPageHelper().paged(sql, params, pageInfo);

		return select(sp.getSql(), sp.getParam(), requiredType);
	}

	/**
	 * 分页查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<String> xxs = select(sql, params, pageInfo, String.class)};
	 *    {@code List<Long> xxs = select(sql, params, pageInfo, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	public <T> List<T> select(String sql, Map<String, Object> params, PageInfo pageInfo,
			Class<T> requiredType) throws DataAccessException {
		return select(sql, new MapSqlParameterSource(params), pageInfo, requiredType);
	}

	/**
	 * 分页查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<String> xxs = select(sql, params, pageInfo, String.class)};
	 *    {@code List<Long> xxs = select(sql, params, pageInfo, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	public <T> List<T> select(String sql, SqlParameterSource params, PageInfo pageInfo,
			Class<T> requiredType) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(pageInfo, "pageInfo不能为空!");
		Assert.notNull(requiredType, "requiredType!");

		int cnt = queryForInt("select count(1) from (" + sql + ") total", params);

		pageInfo.setTotal(cnt);

		SqlAndParam<SqlParameterSource> sp = getPageHelper().paged(sql, params,
				pageInfo);

		return select(sp.getSql(), sp.getParam(), requiredType);
	}

	/**
	 * 查询指定类型的值列表的分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = pageInfo(sql, pageInfo, String.class, params);
	 *     PageInfo pageInfo = pageInfo(sql, pageInfo, String.class, "王五");
	 *     PageInfo pageInfo = pageInfo(sql, pageInfo, String.class, new Object[] { "王五" });
	 *     PageInfo pageInfo = pageInfo(sql, pageInfo, Long.class, params);
	 * </pre>
	 * @param sql sql语句
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录的分页信息；如查询不到，则返回{@code result}为空的{@code pageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	public <T> PageInfo pageInfo(String sql, PageInfo pageInfo, Class<T> requiredType,
			Object... params) throws DataAccessException {
		return pageInfo(sql, params, pageInfo, requiredType);
	}

	/**
	 * 查询指定类型的值列表的分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = pageInfo(sql, params, pageInfo, String.class);
	 *     PageInfo pageInfo = pageInfo(sql, new Object[] { "王五" }, pageInfo, String.class);
	 *     PageInfo pageInfo = pageInfo(sql, params, pageInfo, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录的分页信息；如查询不到，则返回{@code result}为空的{@code pageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	public <T> PageInfo pageInfo(String sql, Object[] params, PageInfo pageInfo,
			Class<T> requiredType) throws DataAccessException {
		List<T> list = select(sql, params, pageInfo, requiredType);
		pageInfo.setList(list);
		return pageInfo;
	}

	/**
	 * 查询指定类型的值列表的分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = pageInfo(sql, params, pageInfo, String.class);
	 *     PageInfo pageInfo = pageInfo(sql, params, pageInfo, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录的分页信息；如查询不到，则返回{@code result}为空的{@code pageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	public <T> PageInfo pageInfo(String sql, Map<String, Object> params,
			PageInfo pageInfo, Class<T> requiredType) throws DataAccessException {
		List<T> list = select(sql, params, pageInfo, requiredType);
		pageInfo.setList(list);
		return pageInfo;
	}

	/**
	 * 查询指定类型的值列表的分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = pageInfo(sql, params, pageInfo, String.class);
	 *     PageInfo pageInfo = pageInfo(sql, params, pageInfo, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录的分页信息；如查询不到，则返回{@code result}为空的{@code pageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 * @see org.springframework.jdbc.core.namedparam.EmptySqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
	 * @see MapSqlParameterSource
	 */
	public <T> PageInfo pageInfo(String sql, SqlParameterSource params, PageInfo pageInfo,
			Class<T> requiredType) throws DataAccessException {
		List<T> list = select(sql, params, pageInfo, requiredType);
		pageInfo.setList(list);
		return pageInfo;
	}

	/**
	 * 查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code List<XxPo> xxPos = select(sql, BeanPropertyRowMapper.newInstance(XxPo.class), params)};
	 *    {@code List<String> xxs = select(sql, SingleColumnRowMapper.newInstance(String.class), "王五")};
	 *    {@code List<String> xxs = select(sql, SingleColumnRowMapper.newInstance(String.class), new Object[] { "王五" })};
	 *    {@code List<Map<String, Object>> xxs = select(sql, new ColumnMapRowMapper(), "王五")};
	 *    {@code List<Map<String, Object>> xxs = select(sql, new ColumnMapRowMapper(), params)};
	 * </pre>
	 * @param sql sql语句
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	public <T> List<T> select(String sql, RowMapper<T> rowMapper, Object... params)
			throws DataAccessException {
		return select(sql, params, rowMapper);
	}

	/**
	 * 查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code List<XxPo> xxPos = select(sql, params, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *    {@code List<String> xxs = select(sql, new Object[] { "王五" }, 为SingleColumnRowMapper.newInstance(String.class))};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see SingleColumnRowMapper
	 * @see BeanPropertyRowMapper
	 * @see ColumnMapRowMapper
	 */
	public <T> List<T> select(String sql, Object[] params, RowMapper<T> rowMapper)
			throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(rowMapper, "rowMapper不能为空!");

		printSql(new SqlAndParam<>(sql, params));

		return getJdbcTemplate().query(sql, params, rowMapper);
	}

	/**
	 * 查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *    {@code List<XxPo> xxPos = select(sql, params, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *    {@code List<String> xxs = select(sql, params, 为SingleColumnRowMapper.newInstance(String.class))};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see SingleColumnRowMapper
	 * @see BeanPropertyRowMapper
	 * @see ColumnMapRowMapper
	 */
	public <T> List<T> select(String sql, Map<String, Object> params,
			RowMapper<T> rowMapper) throws DataAccessException {
		return select(sql, new MapSqlParameterSource(params), rowMapper);
	}

	/**
	 * 查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *    {@code List<XxPo> xxPos = select(sql, params, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *    {@code List<String> xxs = select(sql, params, 为SingleColumnRowMapper.newInstance(String.class))};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see SingleColumnRowMapper
	 * @see BeanPropertyRowMapper
	 * @see ColumnMapRowMapper
	 */
	public <T> List<T> select(String sql, SqlParameterSource params,
			RowMapper<T> rowMapper) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(rowMapper, "rowMapper不能为空!");

		printSql(new SqlAndParam<>(sql, params));

		return getNamedParameterJdbcTemplate().query(sql, params, rowMapper);
	}

	/**
	 * 分页查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(sql, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class), params)};
	 *    {@code List<String> xxs = select(sql, pageInfo, SingleColumnRowMapper.newInstance(String.class), "王五")};
	 *    {@code List<String> xxs = select(sql, pageInfo, SingleColumnRowMapper.newInstance(String.class), new Object[] { "王五" })};
	 *    {@code List<Map<String, Object>> xxs = select(sql, pageInfo, new ColumnMapRowMapper(), "王五")};
	 *    {@code List<Map<String, Object>> xxs = select(sql, pageInfo, new ColumnMapRowMapper(), params)};
	 * </pre>
	 * @param sql sql语句
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	public <T> List<T> select(String sql, PageInfo pageInfo, RowMapper<T> rowMapper,
			Object... params) throws DataAccessException {
		return select(sql, params, pageInfo, rowMapper);
	}

	/**
	 * 分页查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(sql, params, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *    {@code List<String> xxs = select(sql, new Object[] { "王五" }, pageInfo, SingleColumnRowMapper.newInstance(String.class))};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, pageInfo, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see SingleColumnRowMapper
	 * @see BeanPropertyRowMapper
	 * @see ColumnMapRowMapper
	 */
	public <T> List<T> select(String sql, Object[] params, PageInfo pageInfo,
			RowMapper<T> rowMapper) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(pageInfo, "pageInfo不能为空!");
		Assert.notNull(rowMapper, "rowMapper不能为空!");

		int cnt = queryForInt("select count(1) from (" + sql + ") total", params);

		pageInfo.setTotal(cnt);

		SqlAndParam<Object[]> sp = getPageHelper().paged(sql, params, pageInfo);

		return select(sp.getSql(), sp.getParam(), rowMapper);
	}

	/**
	 * 分页查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(sql, params, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *    {@code List<String> xxs = select(sql, params, pageInfo, SingleColumnRowMapper.newInstance(String.class))};
	 *    {@code List<Map<String, Object>> xxs = get(sql, params, pageInfo, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see SingleColumnRowMapper
	 * @see BeanPropertyRowMapper
	 * @see ColumnMapRowMapper
	 */
	public <T> List<T> select(String sql, Map<String, Object> params, PageInfo pageInfo,
			RowMapper<T> rowMapper) throws DataAccessException {
		return select(sql, new MapSqlParameterSource(params), pageInfo, rowMapper);
	}

	/**
	 * 分页查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(sql, params, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *    {@code List<String> xxs = select(sql, params, pageInfo, SingleColumnRowMapper.newInstance(String.class))};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, pageInfo, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see SingleColumnRowMapper
	 * @see BeanPropertyRowMapper
	 * @see ColumnMapRowMapper
	 */
	public <T> List<T> select(String sql, SqlParameterSource params, PageInfo pageInfo,
			RowMapper<T> rowMapper) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(pageInfo, "pageInfo不能为空!");
		Assert.notNull(rowMapper, "rowMapper不能为空!");

		int cnt = queryForInt("select count(1) from (" + sql + ") total", params);

		pageInfo.setTotal(cnt);

		SqlAndParam<SqlParameterSource> sp = getPageHelper().paged(sql, params,
				pageInfo);

		return select(sp.getSql(), sp.getParam(), rowMapper);
	}

	/**
	 * 查询指定类型的对象列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class), params)};
	 *     PageInfo pageInfo = select(sql, pageInfo, SingleColumnRowMapper.newInstance(String.class), "王五")};
	 *     PageInfo pageInfo = select(sql, pageInfo, SingleColumnRowMapper.newInstance(String.class), new Object[] { "王五" })};
	 *     PageInfo pageInfo = select(sql, pageInfo, new ColumnMapRowMapper(), "王五")};
	 *     PageInfo pageInfo = select(sql, pageInfo, new ColumnMapRowMapper(), params)};
	 * </pre>
	 * @param sql sql语句
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql及分页能查询到记录，则以集合形式存储在{@code PageInfo的list}中；如查询不到，则返回{@code list}为null的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	public <T> PageInfo pageInfo(String sql, PageInfo pageInfo, RowMapper<T> rowMapper,
			Object... params) throws DataAccessException {
		return pageInfo(sql, params, pageInfo, rowMapper);
	}

	/**
	 * 查询指定类型的对象列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, params, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *     PageInfo pageInfo = select(sql, new Object[] { "王五" }, pageInfo, SingleColumnRowMapper.newInstance(String.class))};
	 *     PageInfo pageInfo = select(sql, params, pageInfo, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo
	 * @return 如该sql及分页能查询到记录，则以集合形式存储在{@code PageInfo的list}中；如查询不到，则返回{@code list}为null的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see SingleColumnRowMapper
	 * @see BeanPropertyRowMapper
	 * @see ColumnMapRowMapper
	 */
	public <T> PageInfo pageInfo(String sql, Object[] params, PageInfo pageInfo,
			RowMapper<T> rowMapper) throws DataAccessException {
		List<T> list = select(sql, params, pageInfo, rowMapper);
		pageInfo.setList(list);
		return pageInfo;
	}

	/**
	 * 查询指定类型的对象列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, params, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *     PageInfo pageInfo = select(sql, params, pageInfo, SingleColumnRowMapper.newInstance(String.class))};
	 *     PageInfo pageInfo = get(sql, params, pageInfo, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo
	 * @return 如该sql及分页能查询到记录，则以集合形式存储在{@code PageInfo的list}中；如查询不到，则返回{@code list}为null的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see SingleColumnRowMapper
	 * @see BeanPropertyRowMapper
	 * @see ColumnMapRowMapper
	 */
	public <T> PageInfo pageInfo(String sql, Map<String, Object> params,
			PageInfo pageInfo, RowMapper<T> rowMapper) throws DataAccessException {
		List<T> list = select(sql, params, pageInfo, rowMapper);
		pageInfo.setList(list);
		return pageInfo;
	}

	/**
	 * 查询指定类型的对象列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, params, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class));
	 *     PageInfo pageInfo = select(sql, params, pageInfo, SingleColumnRowMapper.newInstance(String.class));
	 *     PageInfo pageInfo xxs = select(sql, params, pageInfo, new ColumnMapRowMapper());
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo
	 * @return 如该sql及分页能查询到记录，则以集合形式存储在{@code PageInfo的list}中；如查询不到，则返回{@code list}为null的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see SingleColumnRowMapper
	 * @see BeanPropertyRowMapper
	 * @see ColumnMapRowMapper
	 */
	public <T> PageInfo pageInfo(String sql, SqlParameterSource params, PageInfo pageInfo,
			RowMapper<T> rowMapper) throws DataAccessException {
		List<T> list = select(sql, params, pageInfo, rowMapper);
		pageInfo.setList(list);
		return pageInfo;
	}

	/**
	 * 查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, params)};
	 *    {@code List<Map<String, Object>> xxs = select(sql, "王五")};
	 *    {@code List<Map<String, Object>> xxs = select(sql, new Object[] { "王五" })};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public List<Map<String, Object>> select(String sql, Object... params)
			throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");

		printSql(new SqlAndParam<>(sql, params));

		return getJdbcTemplate().queryForList(sql, params);
	}

	/**
	 * 查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, params)};
	 *    {@code List<Map<String, Object>> xxs = select(sql, new Object[] { "王五" })};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public List<Map<String, Object>> select(String sql, Map<String, Object> params)
			throws DataAccessException {
		return select(sql, new MapSqlParameterSource(params));
	}

	/**
	 * 查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, params)};
	 *    {@code List<Map<String, Object>> xxs = select(sql, new Object[] { "王五" })};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
	 * @see MapSqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.EmptySqlParameterSource
	 */
	public List<Map<String, Object>> select(String sql, SqlParameterSource params)
			throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");

		printSql(new SqlAndParam<>(sql, params));

		return getNamedParameterJdbcTemplate().queryForList(sql, params);
	}

	/**
	 * 分页查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, pageInfo, params)};
	 *    {@code List<Map<String, Object>> xxs = select(sql, pageInfo, "王五")};
	 *    {@code List<Map<String, Object>> xxs = select(sql, pageInfo, new Object[] { "王五" })};
	 * </pre>
	 * @param sql sql语句
	 * @param pageInfo 分页信息
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public List<Map<String, Object>> select(String sql, PageInfo pageInfo,
			Object... params) throws DataAccessException {
		return select(sql, params, pageInfo);
	}

	/**
	 * 分页查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, pageInfo)};
	 *    {@code List<Map<String, Object>> xxs = select(sql, new Object[] { "王五" }, pageInfo)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public List<Map<String, Object>> select(String sql, Object[] params,
			PageInfo pageInfo) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(pageInfo, "pageInfo不能为空!");

		int cnt = queryForInt("select count(1) from (" + sql + ")  total", params);
		pageInfo.setTotal(cnt);

		SqlAndParam<Object[]> sp = getPageHelper().paged(sql, params, pageInfo);

		printSql(sp);

		return getJdbcTemplate().queryForList(sp.getSql(), sp.getParam());
	}

	/**
	 * 分页查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, pageInfo)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	public List<Map<String, Object>> select(String sql, Map<String, Object> params,
			PageInfo pageInfo) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(pageInfo, "pageInfo不能为空!");

		int cnt = queryForInt("select count(1) from (" + sql + ") total", params);
		pageInfo.setTotal(cnt);

		SqlAndParam<Map<String, Object>> sp = getPageHelper().paged(sql, params,
				pageInfo);

		printSql(sp);

		return getNamedParameterJdbcTemplate().queryForList(sp.getSql(), sp.getParam());
	}

	/**
	 * 分页查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, pageInfo)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 * @see org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
	 * @see MapSqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.EmptySqlParameterSource
	 */
	public List<Map<String, Object>> select(String sql, SqlParameterSource params,
			PageInfo pageInfo) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(pageInfo, "pageInfo不能为空!");

		int cnt = queryForInt("select count(1) from (" + sql + ") total", params);
		pageInfo.setTotal(cnt);

		SqlAndParam<SqlParameterSource> sp = getPageHelper().paged(sql, params,
				pageInfo);

		printSql(sp);

		return getNamedParameterJdbcTemplate().queryForList(sp.getSql(), sp.getParam());
	}

	/**
	 * 查询Map列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, pageInfo, params);
	 *     PageInfo pageInfo = select(sql, pageInfo, "王五");
	 *     PageInfo pageInfo = select(sql, pageInfo, new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param pageInfo 分页信息
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code result}为空的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public PageInfo pageInfo(String sql, PageInfo pageInfo, Object... params)
			throws DataAccessException {
		return pageInfo(sql, params, pageInfo);
	}

	/**
	 * 查询Map列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, params, pageInfo);
	 *     PageInfo pageInfo = select(sql, new Object[] { "王五" }, pageInfo);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code result}为空的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	public PageInfo pageInfo(String sql, Object[] params, PageInfo pageInfo)
			throws DataAccessException {
		List<Map<String, Object>> list = select(sql, params, pageInfo);
		pageInfo.setList(list);
		return pageInfo;
	}

	/**
	 * 查询Map列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, params, pageInfo);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code result}为空的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	public PageInfo pageInfo(String sql, Map<String, Object> params, PageInfo pageInfo)
			throws DataAccessException {
		List<Map<String, Object>> list = select(sql, params, pageInfo);
		pageInfo.setList(list);
		return pageInfo;
	}

	/**
	 * 查询Map列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, params, pageInfo);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code result}为空的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 * @see org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
	 * @see MapSqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.EmptySqlParameterSource
	 */
	public PageInfo pageInfo(String sql, SqlParameterSource params, PageInfo pageInfo)
			throws DataAccessException {
		List<Map<String, Object>> list = select(sql, params, pageInfo);
		pageInfo.setList(list);
		return pageInfo;
	}

	/**
	 * 查询int <pre>
	 *     int age = queryForInt("select age from user where name = ?", "王五");
	 *     int age = queryForInt("select age from user where name = ?", new Object[] { "王五" });
	 *     int count = queryForInt("select count(*) from user where name = ?", new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的int值，默认为{@code 0}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回0
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Integer类型，会抛出此异常
	 */
	public int queryForInt(String sql, Object... params) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");

		Integer res = queryForObject(sql, params, Integer.class);

		return res == null ? Integer.valueOf(0) : res.intValue();
	}

	/**
	 * 查询int <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     int age = queryForInt("select age from user where name = :name", params);
	 *     int count = queryForInt("select count(*) from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的int值，默认为{@code 0}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回0
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Integer类型，会抛出此异常
	 */
	public int queryForInt(String sql, Map<String, Object> params)
			throws DataAccessException {
		return queryForInt(sql, new MapSqlParameterSource(params));
	}

	/**
	 * 查询int <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     int age = queryForInt("select age from user where name = :name", params);
	 *     int count = queryForInt("select count(*) from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的int值，默认为{@code 0}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回0
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Integer类型，会抛出此异常
	 */
	public int queryForInt(String sql, SqlParameterSource params)
			throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");

		Integer res = queryForObject(sql, params, Integer.class);

		return res == null ? Integer.valueOf(0) : res.intValue();
	}

	/**
	 * 查询Long <pre>
	 *     Long amount = queryForLong("select amount from user where name = ?", "王五");
	 *     Long amount = queryForLong("select amount from user where name = ?", new Object[] { "王五" });
	 *     Long count = queryForLong("select count(*) from user where name = ?", new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的Long值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Long类型，会抛出此异常
	 */
	public Long queryForLong(String sql, Object... params) throws DataAccessException {
		return queryForObject(sql, params, Long.class);
	}

	/**
	 * 查询Long <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     Long amount = queryForLong("select amount from user where name = :name", params);
	 *     Long count = queryForLong("select count(*) from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的Long值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Long类型，会抛出此异常
	 */
	public Long queryForLong(String sql, Map<String, Object> params)
			throws DataAccessException {
		return queryForObject(sql, params, Long.class);
	}

	/**
	 * 查询Long <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     Long amount = queryForLong("select amount from user where name = :name", params);
	 *     Long count = queryForLong("select count(*) from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的Long值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Long类型，会抛出此异常
	 */
	public Long queryForLong(String sql, SqlParameterSource params)
			throws DataAccessException {
		return queryForObject(sql, params, Long.class);
	}

	/**
	 * 查询Double <pre>
	 *     Double amount = queryForDouble("select amount from user where name = ?", "王五");
	 *     Double amount = queryForDouble("select amount from user where name = ?", new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的Double值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Double类型，会抛出此异常
	 */
	public Double queryForDouble(String sql, Object... params)
			throws DataAccessException {
		return queryForObject(sql, params, Double.class);
	}

	/**
	 * 查询Double <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     Double amount = queryForDouble("select amount from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的Double值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Double类型，会抛出此异常
	 */
	public Double queryForDouble(String sql, Map<String, Object> params)
			throws DataAccessException {
		return queryForObject(sql, params, Double.class);
	}

	/**
	 * 查询Double <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     Double amount = queryForDouble("select amount from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的Double值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Double类型，会抛出此异常
	 */
	public Double queryForDouble(String sql, SqlParameterSource params)
			throws DataAccessException {
		return queryForObject(sql, params, Double.class);
	}

	/**
	 * 查询BigDecimal <pre>
	 *     BigDecimal amount = queryForBigDecimal("select amount from user where name = ?", "王五");
	 *     BigDecimal amount = queryForBigDecimal("select amount from user where name = ?", new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的BigDecimal值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为BigDecimal类型，会抛出此异常
	 */
	public BigDecimal queryForBigDecimal(String sql, Object... params)
			throws DataAccessException {
		return queryForObject(sql, params, BigDecimal.class);
	}

	/**
	 * 查询BigDecimal <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     BigDecimal amount = queryForBigDecimal("select amount from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的BigDecimal值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为BigDecimal类型，会抛出此异常
	 */
	public BigDecimal queryForBigDecimal(String sql, Map<String, Object> params)
			throws DataAccessException {
		return queryForObject(sql, params, BigDecimal.class);
	}

	/**
	 * 查询BigDecimal <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     BigDecimal amount = queryForBigDecimal("select amount from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的BigDecimal值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为BigDecimal类型，会抛出此异常
	 */
	public BigDecimal queryForBigDecimal(String sql, SqlParameterSource params)
			throws DataAccessException {
		return queryForObject(sql, params, BigDecimal.class);
	}

	/**
	 * 查询String <pre>
	 *     String xx = queryForString("select xx from user where name = ?", "王五");
	 *     String xx = queryForString("select xx from user where name = ?", new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的String值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为String类型，会抛出此异常
	 */
	public String queryForString(String sql, Object... params)
			throws DataAccessException {
		return queryForObject(sql, params, String.class);
	}

	/**
	 * 查询String <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     String xx = queryForString("select xx from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的String值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为String类型，会抛出此异常
	 */
	public String queryForString(String sql, Map<String, Object> params)
			throws DataAccessException {
		return queryForObject(sql, params, String.class);
	}

	/**
	 * 查询String <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     String xx = queryForString("select xx from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的String值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为String类型，会抛出此异常
	 */
	public String queryForString(String sql, SqlParameterSource params)
			throws DataAccessException {
		return queryForObject(sql, params, String.class);
	}

	/**
	 * 查询指定类型的值 <pre>
	 *     String xx = queryForObject("select xx from user where name = ?", String.class, "王五");
	 *     String xx = queryForObject("select xx from user where name = ?", String.class, new Object[] { "王五" });
	 *     Long xx = queryForObject("select xx from user where name = ?", Long.class, new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param requiredType 指定值类型的Class
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	public <T> T queryForObject(String sql, Class<T> requiredType, Object... params)
			throws DataAccessException {
		return queryForObject(sql, params, requiredType);
	}

	/**
	 * 查询指定类型的值 <pre>
	 *     String xx = queryForObject("select xx from user where name = ?", new Object[] { "王五" }, String.class);
	 *     Long xx = queryForObject("select xx from user where name = ?", new Object[] { "王五" }, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	public <T> T queryForObject(String sql, Object[] params, Class<T> requiredType)
			throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(requiredType, "requiredType不能为空!");

		printSql(new SqlAndParam<>(sql, params));

		try {
			return getJdbcTemplate().queryForObject(sql, params, requiredType);
		}
		catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	/**
	 * 查询指定类型的值 <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     String xx = queryForObject("select xx from user where name = :name", params, String.class);
	 *     Long xx = queryForObject("select xx from user where name = :name", params, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	public <T> T queryForObject(String sql, Map<String, Object> params,
			Class<T> requiredType) throws DataAccessException {
		return queryForObject(sql, new MapSqlParameterSource(params), requiredType);
	}

	/**
	 * 查询指定类型的值 <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     String xx = queryForObject("select xx from user where name = :name", params, String.class);
	 *     Long xx = queryForObject("select xx from user where name = :name", params, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，返回空
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	public <T> T queryForObject(String sql, SqlParameterSource params,
			Class<T> requiredType) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(requiredType, "requiredType不能为空!");

		printSql(new SqlAndParam<>(sql, params));

		try {
			return getNamedParameterJdbcTemplate().queryForObject(sql, params,
					requiredType);
		}
		catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String xx = queryForObject("select xx from user where name = ?", new SingleColumnRowMapper(String.class), "王五");
	 *     String xx = queryForObject("select xx from user where name = ?", new SingleColumnRowMapper(String.class), new Object[] { "王五" });
	 *     Long xx = queryForObject("select xx from user where name = ?", new SingleColumnRowMapper(Long.class), new Object[] { "王五" });
	 *     XxPo xxPo = queryForObject("select xx from user where name = ?", BeanPropertyRowMapper.newInstance(XxPo.class), "王五");
	 * </pre>
	 * @param sql sql语句
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	public <T> T queryForObject(String sql, RowMapper<T> rowMapper, Object... params)
			throws DataAccessException {
		return queryForObject(sql, params, rowMapper);
	}

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String xx = queryForObject("select xx from user where name = ?", new Object[] { "王五" }, new SingleColumnRowMapper(String.class));
	 *     Long xx = queryForObject("select xx from user where name = ?", new Object[] { "王五" }, new SingleColumnRowMapper(Long.class));
	 *     XxPo xxPo = queryForObject("select xx from user where name = ?", new Object[] { "王五" }, BeanPropertyRowMapper.newInstance(XxPo.class));
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	public <T> T queryForObject(String sql, Object[] params, RowMapper<T> rowMapper)
			throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(rowMapper, "rowMapper不能为空!");

		printSql(new SqlAndParam<>(sql, params));

		try {
			return getJdbcTemplate().queryForObject(sql, params, rowMapper);
		}
		catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     String xx = queryForObject("select xx from user where name = :name", params, new SingleColumnRowMapper(String.class));
	 *     Long xx = queryForObject("select xx from user where name = :name", params, new SingleColumnRowMapper(Long.class));
	 *     XxPo xxPo = queryForObject("select xx from user where name = :name", params, BeanPropertyRowMapper.newInstance(XxPo.class));
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	public <T> T queryForObject(String sql, Map<String, Object> params,
			RowMapper<T> rowMapper) throws DataAccessException {
		return queryForObject(sql, new MapSqlParameterSource(params), rowMapper);
	}

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     String xx = queryForObject("select xx from user where name = :name", params, new SingleColumnRowMapper(String.class));
	 *     Long xx = queryForObject("select xx from user where name = :name", params, new SingleColumnRowMapper(Long.class));
	 *     XxPo xxPo = queryForObject("select xx from user where name = :name", params, BeanPropertyRowMapper.newInstance(XxPo.class));
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	public <T> T queryForObject(String sql, SqlParameterSource params,
			RowMapper<T> rowMapper) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");
		Assert.notNull(rowMapper, "rowMapper不能为空!");

		printSql(new SqlAndParam<>(sql, params));

		try {
			return getNamedParameterJdbcTemplate().queryForObject(sql, params, rowMapper);
		}
		catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	/**
	 * 数据库更新操作
	 *
	 * <p>
	 * {@code insert}、{@code update}、{@code delete}操作均可调用此方法。
	 * </p>
	 * <pre>
	 *     update("insert into tableName (colum) values (?)"), new Object[] { "1122"})
	 *     update("update tableName set colum = :colum where colum = ?"), "1122")
	 *     update("delete from tableName where colum = ?"), new Object[] { "1122"})
	 * </pre>
	 * @param sql sql语句
	 * @param params 参数列表
	 * @return 执行成功行数，如全部失败，则返回{@code 0}
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public int update(String sql, Object... params) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");

		printSql(new SqlAndParam<>(sql, params));

		return getJdbcTemplate().update(sql, params);
	}

	/**
	 * 数据库更新操作
	 *
	 * <p>
	 * {@code insert}、{@code update}、{@code delete}操作均可调用此方法。
	 * </p>
	 * <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("colum", "1122");
	 *
	 *     update("insert into tableName (colum) values (:colum)"), params)
	 *     update("update tableName set colum = :colum where colum = :colum"), params)
	 *     update("delete from tableName where colum = :colum"), params)
	 * </pre>
	 * @param sql sql语句
	 * @param params 参数列表
	 * @return 执行成功行数，如全部失败，则返回{@code 0}
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	public int update(String sql, Map<String, Object> params) throws DataAccessException {
		return update(sql, new MapSqlParameterSource(params));
	}

	/**
	 * 数据库更新操作
	 *
	 * <p>
	 * {@code insert}、{@code update}、{@code delete}操作均可调用此方法。
	 * </p>
	 * <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("colum", "1122");
	 *
	 *     update("insert into tableName (colum) values (:colum)"), params)
	 *     update("update tableName set colum = :colum where colum = :colum"), params)
	 *     update("delete from tableName where colum = :colum"), params)
	 * </pre>
	 * @param sql sql语句
	 * @param params 参数列表
	 * @return 执行成功行数，如全部失败，则返回{@code 0}
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.namedparam.EmptySqlParameterSource
	 * @see MapSqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
	 */
	public int update(String sql, SqlParameterSource params) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空!");
		Assert.notNull(params, "params不能为空!");

		printSql(new SqlAndParam<>(sql, params));

		return getNamedParameterJdbcTemplate().update(sql, params);
	}

	/**
	 * 批量数据库更新操作
	 *
	 * <p>
	 * {@code insert}、{@code update}、{@code delete}操作均可调用此方法。
	 * </p>
	 * <pre>
	 *     // {@code List<Object[]>}方式
	 *     Object[] params_1 = new Object[] { "王五" };
	 *     Object[] params_2 = new Object[] { "李四" };
	 *
	 *     {@code List<Object[]> params = new ArrayList<>()};
	 *     params.add(params_1);
	 *     params.add(params_2);
	 *
	 *     // SqlParameterSource的MapSqlParameterSource方式
	 *     MapSqlParameterSource params_1 = new MapSqlParameterSource();
	 *     params.addValue("colum", "1122");
	 *
	 *     MapSqlParameterSource params_2 = new MapSqlParameterSource();
	 *     params.addValue("colum", "3344");
	 *
	 *     {@code List<MapSqlParameterSource> params = new ArrayList<>()};
	 *     params.add(params_1);
	 *     params.add(params_2);
	 *
	 *     XxPo xxPo_1 = new XxPo();
	 *     xxPo_1.setName("王五");
	 *
	 *     XxPo xxPo_2 = new XxPo();
	 *     xxPo_2.setName("李四");
	 *
	 *     // SqlParameterSource的BeanPropertySqlParameterSource方式
	 *     BeanPropertySqlParameterSource params_1 = new BeanPropertySqlParameterSource(xxPo_1);
	 *     BeanPropertySqlParameterSource params_2 = new BeanPropertySqlParameterSource(xxPo_2);
	 *
	 *     {@code List<BeanPropertySqlParameterSource> params = new ArrayList<>()};
	 *     params.add(params_1);
	 *     params.add(params_2);
	
	 *
	 *     update("insert into tableName (colum) values (:colum)"), params);
	 *     update("update tableName set colum = :colum where colum = :colum"), params);
	 *     update("delete from tableName where colum = :colum"), params);
	 *
	 *
	 * </pre>
	 * @param sql sql语句
	 * @param params 参数列表
	 * @return 执行成功行数，如全部失败，则返回{@code 0}
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.namedparam.EmptySqlParameterSource
	 * @see MapSqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils
	 */
	public int batchUpdate(String sql, List<?> params) throws DataAccessException {
		Assert.notNull(sql, "sql不能为空！");
		Assert.notNull(params, "params不能为空！");
		if (params.size() == 0) {
			throw new IllegalArgumentException("params不能为空！");
		}
		else {
			int[] lines;
			if (params.get(0) instanceof Map) {
				Map[] paramMaps = params.toArray(new HashMap[params.size()]);
				Iterator<?> iterator = params.iterator();

				while (iterator.hasNext()) {
					printSql(new SqlAndParam(sql, iterator.next()));
				}

				lines = getNamedParameterJdbcTemplate().batchUpdate(sql, paramMaps);
			}
			else if (params.get(0) instanceof SqlParameterSource) {
				SqlParameterSource[] sqlParameterSources = (SqlParameterSource[]) params
						.toArray(new HashMap[params.size()]);
				Iterator<?> iterator = params.iterator();

				while (iterator.hasNext()) {
					printSql(new SqlAndParam(sql, iterator.next()));
				}

				lines = getNamedParameterJdbcTemplate().batchUpdate(sql,
						sqlParameterSources);
			}
			else if (params.get(0).getClass().isArray()) {
				Iterator iterator = params.iterator();

				while (iterator.hasNext()) {
					this.printSql(new SqlAndParam(sql, iterator.next()));
				}

				lines = getJdbcTemplate().batchUpdate(sql, (List<Object[]>) params);
			}
			else {
				throw new IllegalArgumentException("params参数类型不合法！");
			}

			int line = 0;
			if (lines != null) {
				line = Arrays.stream(lines).sum();
			}

			return line;
		}
	}

	private <T> void printSql(SqlAndParam<T> sp) {
		if (isShowSql()) {
			this.logger.info("{}", sp.getSql());
		}
	}

}
