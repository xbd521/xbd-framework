package org.xbdframework.dao.support.pagehelper;

import org.xbdframework.dao.DbType;
import org.xbdframework.dao.PageHelper;

import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.util.StringUtils;

/**
 * SpringJdbc方式分页实现{@link PageHelper}
 *
 * <p>配置使用时，请根据数据库类型{@link DbType}创建对应的{@code PageHelper}.
 * <p>可能数据库不同，其所支持的查询某些行记录的方法也不同，可根据声明的数据库类型，组织不同的行记录限定条件
 *
 * @author luas
 * @since 4.3
 * @see DbType
 */
public abstract class JdbcPageHelper implements PageHelper {

	protected MapSqlParameterSource buildParameters(SqlParameterSource parameterSource) {
		MapSqlParameterSource result = new MapSqlParameterSource();

		String[] parameters = extractParameters(parameterSource);

		if (parameters == null || parameters.length == 0) {
			return result;
		}

		for (String parameterName : parameters) {
			if (parameterSource.hasValue(parameterName)) {
				result.addValue(parameterName, parameterSource.getValue(parameterName));
			}
		}

		return result;
	}

	protected String[] extractParameters(SqlParameterSource parameters) {
		if (parameters instanceof BeanPropertySqlParameterSource) {
			return ((BeanPropertySqlParameterSource) parameters)
					.getReadablePropertyNames();
		}
		else if (parameters instanceof MapSqlParameterSource) {
			return StringUtils.toStringArray(
					((MapSqlParameterSource) parameters).getValues().keySet());
		}

		return null;
	}

}
