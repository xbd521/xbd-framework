package org.xbdframework.dao;

import java.util.Map;

import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import org.xbdframework.dao.core.PageInfo;
import org.xbdframework.jdbc.core.SqlAndParam;

/**
 * 分页列表查询接口定义
 *
 * <p>
 * 可能数据库不同，其所支持的查询某些行记录的方法也不同，可根据声明的数据库类型，组织不同的行记录限定条件
 * </p>
 *
 * @author luas
 * @since 4.3
 * @see DbType
 */
public interface PageHelper {

	/**
	 * 构造分页sql
	 * @param sql sql语句
	 * @param parameters sql中where条件对应的参数值
	 * @param pageInfo 分页信息
	 * @return Map类型的SqlAndParams
	 */
	SqlAndParam<Map<String, Object>> paged(String sql,
										   Map<String, Object> parameters, PageInfo pageInfo);

	/**
	 * 构造分页sql
	 * @param sql sql语句
	 * @param parameters sql中where条件对应的参数值
	 * @param pageInfo 分页信息
	 * @return SqlParameterSource类型的SqlAndParams
	 */
	SqlAndParam<SqlParameterSource> paged(String sql, SqlParameterSource parameters,
										  PageInfo pageInfo);

	/**
	 * 构造分页sql
	 * @param sql sql语句
	 * @param parameters sql中where条件对应的参数值
	 * @param pageInfo 分页信息
	 * @return Object[]类型的SqlAndParams
	 */
	SqlAndParam<Object[]> paged(String sql, Object[] parameters, PageInfo pageInfo);

}
