package org.xbdframework.dao.core.support;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import org.xbdframework.dao.PageHelper;
import org.xbdframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

/**
 * Extension of NamedParameterJdbcDaoSupport that exposes a PageHelper and show sql symbol
 * as well.
 *
 * @author 刘明磊
 * @since 4.3
 * @see NamedParameterJdbcTemplate
 */
public class XbdJdbcDaoSupport extends NamedParameterJdbcDaoSupport {

	private PageHelper pageHelper;

	private boolean showSql = true;

	@Override
	protected void checkDaoConfig() {
		super.checkDaoConfig();

		if (this.pageHelper == null) {
			throw new IllegalArgumentException("'pageHelper' is required.");
		}
	}

	public final PageHelper getPageHelper() {
		return pageHelper;
	}

	public final void setPageHelper(PageHelper pageHelper) {
		this.pageHelper = pageHelper;
	}

	public final boolean isShowSql() {
		return showSql;
	}

	public final void setShowSql(boolean showSql) {
		this.showSql = showSql;
	}

}
