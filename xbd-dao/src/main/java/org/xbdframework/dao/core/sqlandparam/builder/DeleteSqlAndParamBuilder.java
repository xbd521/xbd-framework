package org.xbdframework.dao.core.sqlandparam.builder;

import java.util.Map;

import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import org.xbdframework.dao.core.BasePo;
import org.xbdframework.dao.core.sqlandparam.AbstractSqlAndParamBuilder;
import org.xbdframework.jdbc.core.SqlAndParam;

public class DeleteSqlAndParamBuilder<T extends BasePo>
		extends AbstractSqlAndParamBuilder<T> {

	public DeleteSqlAndParamBuilder(T po) {
		super(po);
	}

	@Override
	public SqlAndParam<Object[]> buildArraySql() {
		StringBuilder sb = new StringBuilder("delete from ")
				.append(this.po.getTableName()).append(" where ")
				.append(this.po.getPkName()).append(" = ?");

		return new SqlAndParam<>(sb.toString(), buildPkWhereArrayParams());
	}

	@Override
	public SqlAndParam<Object[]> buildArraySql(String where, Object[] params) {
		return new SqlAndParam<>(new StringBuilder().append("delete from ")
				.append(this.po.getTableName()).append(escapeWhere(where)).toString(),
				params);
	}

	@Override
	public SqlAndParam<Map<String, Object>> buildMapSql() {
		StringBuilder sb = new StringBuilder("delete from ")
				.append(this.po.getTableName()).append(" where ")
				.append(this.po.getPkName()).append(" = :").append(this.po.getPkName());

		return new SqlAndParam<>(sb.toString(), buildPkWhereMapParams());
	}

	@Override
	public SqlAndParam<Map<String, Object>> buildMapSql(String where,
			Map<String, Object> paramMap) {
		return new SqlAndParam<>(new StringBuilder().append("delete from ")
				.append(this.po.getTableName()).append(escapeWhere(where)).toString(),
				paramMap);
	}

	@Override
	public SqlAndParam<SqlParameterSource> buildSqlParameterSourceSql() {
		StringBuilder sb = new StringBuilder("delete from ")
				.append(this.po.getTableName()).append(" where ")
				.append(this.po.getPkName()).append(" = :").append(this.po.getPkName());

		return new SqlAndParam<>(sb.toString(), buildPkWhereSqlParameterSourceParams());
	}

	@Override
	public SqlAndParam<SqlParameterSource> buildSqlParameterSourceSql(String where,
			SqlParameterSource params) {
		return new SqlAndParam<>(new StringBuilder().append("delete from ")
				.append(this.po.getTableName()).append(escapeWhere(where)).toString(),
				params);
	}

}
