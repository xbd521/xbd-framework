package org.xbdframework.dao.core.sqlandparam;

import java.util.Map;

import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import org.xbdframework.jdbc.core.SqlAndParam;

public interface SqlAndParamBuilder {

	SqlAndParam<Object[]> buildArraySql();

	SqlAndParam<Object[]> buildArraySql(String where, Object[] params);

	SqlAndParam<Map<String, Object>> buildMapSql();

	SqlAndParam<Map<String, Object>> buildMapSql(String where,
			Map<String, Object> params);

	SqlAndParam<SqlParameterSource> buildSqlParameterSourceSql();

	SqlAndParam<SqlParameterSource> buildSqlParameterSourceSql(String where,
			SqlParameterSource params);

}
