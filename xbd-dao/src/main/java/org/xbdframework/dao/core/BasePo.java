package org.xbdframework.dao.core;

import java.io.Serializable;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import org.xbdframework.jdbc.core.SqlAndParam;

public abstract class BasePo implements Serializable, Cloneable {

	public abstract SqlAndParam<Map<String, Object>> getInsertSql();

	public abstract SqlAndParam<Map<String, Object>> getUpdateSql();

	public abstract SqlAndParam<Object[]> getUpdateSql(String where, Object[] params);

	public abstract SqlAndParam<Map<String, Object>> getUpdateSql(String where,
			Map<String, Object> params);

	public abstract SqlAndParam<SqlParameterSource> getUpdateSql(String where,
			SqlParameterSource params);

	public abstract SqlAndParam<Map<String, Object>> getDeleteSql();

	public abstract SqlAndParam<Object[]> getDeleteSql(String where, Object[] params);

	public abstract SqlAndParam<Map<String, Object>> getDeleteSql(String where,
			Map<String, Object> params);

	public abstract SqlAndParam<SqlParameterSource> getDeleteSql(String where,
			SqlParameterSource params);

	public abstract SqlAndParam<Map<String, Object>> getSelectSql();

	public abstract SqlAndParam<Object[]> getSelectSql(String where, Object[] params);

	public abstract SqlAndParam<Map<String, Object>> getSelectSql(String where,
			Map<String, Object> params);

	public abstract SqlAndParam<SqlParameterSource> getSelectSql(String where,
			SqlParameterSource params);

	public abstract String getTableName();

	public abstract String getPkName();

	public abstract Object getPkValue();

	public abstract void setPkValue(Object paramObject);

}
