package org.xbdframework.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import org.xbdframework.dao.core.BasePo;
import org.xbdframework.dao.core.PageInfo;
import org.xbdframework.dao.support.pagehelper.JdbcPageHelper;

/**
 * 持久化、对象获取、单值查询、列表查询等操作接口定义
 *
 * <p>
 * 主要包括po插入（批量）、更新（批量、按条件）、删除（批量、按条件）、
 * 获取（批量、按条件）、Map类型返回值查询、列表、自定义返回值类的单值、
 * 列表获取、分页列表获取等.
 * </p>
 *
 * @author luas
 * @since 4.3
 * @see org.springframework.jdbc.core.JdbcTemplate
 * @see org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
 * @see JdbcPageHelper
 * @see PageHelper
 * @see DbType
 */
public interface BaseDao {

	/**
	 * 插入
	 * <p>
	 * 具体插入的值为Po中已set的字段
	 * </p>
	 * @param basePo PO对象
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 插入成功的行数，如插入失败，则返回{@code 0}；如插入成功，则返回{@code 1}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> int insert(T basePo) throws DataAccessException;

	/**
	 * 插入
	 * <p>
	 * 具体插入的值为Po中已set的字段
	 * </p>
	 * @param basePos PO对象集合
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 插入成功的行数，全部插入失败，返回{@code 0}；
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> int insert(List<T> basePos) throws DataAccessException;

	/**
	 * 按主键更新
	 * <p>
	 * 具体更新的值为Po中已set的字段
	 * </p>
	 * @param basePo PO对象
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 更新成功的行数，如更新失败，则返回{@code 0}；如更新成功，则返回{@code 1}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> int update(T basePo) throws DataAccessException;

	/**
	 * 按主键更新
	 * <p>
	 * 具体更新的值为Po中已set的字段
	 * </p>
	 * @param basePos PO对象集合
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 更新成功的行数，全部更新失败，返回{@code 0}；
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> int update(List<T> basePos) throws DataAccessException;

	/**
	 * 按条件更新
	 * <p>
	 * 需要更新的字段可通过Po进行设置，更新范围通过{@code where}参数进行设置
	 * </p>
	 * <pre>
	 *     XxPo xxPo = new XxPo();
	 *     xxPo.setStatus(2);
	 *     xxPo.setAddUser("张三");
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     update(xxPo, "name = ?", params);
	 *     update(xxPo, "name = ?", "王五");
	 *     update(xxPo, "name = ?", new Object[] { "王五" });
	 *
	 *     生成的sql语句为：update xx set status = 2, adduser = '张三' where name = '王五'
	 * </pre>
	 * @param basePo PO对象集合
	 * @param where sql {@code where}条件，形如{@code " status = :status"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 更新成功的行数。满足条件的行都会被更新，返回其中更新成功的行数。
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> int update(T basePo, String where, Object... params)
			throws DataAccessException;

	/**
	 * 按条件更新
	 * <p>
	 * 需要更新的字段可通过Po进行设置，更新范围通过{@code where}参数进行指定
	 * </p>
	 * <pre>
	 *     XxPo xxPo = new XxPo();
	 *     xxPo.setStatus(2);
	 *     xxPo.setAddUser("张三");
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     update(xxPo, "name = :name", params);
	 *
	 *     生成的sql语句为：update xx set status = 2, adduser = '张三' where name = '王五'
	 * </pre>
	 * @param basePo PO对象集合
	 * @param where sql {@code where}条件，形如{@code " status = :status"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 更新成功的行数。满足条件的行都会被更新，返回其中更新成功的行数。
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> int update(T basePo, String where, Map<String, Object> params)
			throws DataAccessException;

	/**
	 * 按条件更新
	 * <p>
	 * 需要更新的字段可通过Po进行设置，更新范围通过{@code where}参数进行指定
	 * </p>
	 * <pre>
	 *     XxPo xxPo = new XxPo();
	 *     xxPo.setStatus(2);
	 *     xxPo.setAddUser("张三");
	 *
	 *    {@code MapSqlParameterSource params = new MapSqlParameterSource();}
	 *     params.addValue("name", "王五");
	 *
	 *     update(xxPo, "name = :name", params);
	 *
	 *     生成的sql语句为：update xx set status = 2, adduser = '张三' where name = '王五'
	 * </pre>
	 * @param basePo PO对象集合
	 * @param where sql {@code where}条件，形如{@code " status = :status"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 更新成功的行数。满足条件的行都会被更新，返回其中更新成功的行数。
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> int update(T basePo, String where, SqlParameterSource params)
			throws DataAccessException;

	/**
	 * 按主键删除
	 * @param basePo PO对象
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 删除成功的行数，如删除失败，则返回{@code 0}；如删除成功，则返回{@code 1}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> int delete(T basePo) throws DataAccessException;

	/**
	 * 按主键删除
	 * @param basePos PO对象集合
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 删除成功的行数，全部删除失败，返回{@code 0}；
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> int delete(List<T> basePos) throws DataAccessException;

	/**
	 * 按条件删除
	 * <p>
	 * 删除范围通过{@code where}参数进行指定
	 * </p>
	 * <pre>
	 *     XxPo xxPo = new XxPo();
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     update(xxPo, "name = ?", params);
	 *     update(new XxPo(), "name = ?", "王五");
	 *     update(new XxPo(), "name = ?", new Object[] { "王五" });
	 *
	 *     生成的sql语句为：delete from xx where name = '王五'
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :status"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 删除成功的行数。满足条件的行都会被删除，返回其中删除成功的行数。
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> int delete(T basePo, String where, Object... params)
			throws DataAccessException;

	/**
	 * 按条件删除
	 * <p>
	 * 删除范围通过{@code where}参数进行指定
	 * </p>
	 * <pre>
	 *     XxPo xxPo = new XxPo();
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     update(xxPo, "name = ?", params);
	 *
	 *     生成的sql语句为：delete from xx where name = '王五'
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :status"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 删除成功的行数。满足条件的行都会被删除，返回其中删除成功的行数。
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> int delete(T basePo, String where, Map<String, Object> params)
			throws DataAccessException;

	/**
	 * 按条件删除
	 * <p>
	 * 删除范围通过{@code where}参数进行指定
	 * </p>
	 * <pre>
	 *     XxPo xxPo = new XxPo();
	 *
	 *    {@code MapSqlParameterSource params = new MapSqlParameterSource();}
	 *     params.addValue("name", "王五");
	 *
	 *     update(xxPo, "name = ?", params);
	 *
	 *     生成的sql语句为：delete from xx where name = '王五'
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :status"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 删除成功的行数。满足条件的行都会被删除，返回其中删除成功的行数。
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> int delete(T basePo, String where, SqlParameterSource params)
			throws DataAccessException;

	/**
	 * 按主键查询单PO
	 *
	 * <pre>
	 *     XxPo xxPo = get(new XxPo("1122"));
	 * </pre>
	 * @param basePo PO对象
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 如该主键对应的有记录，则返回该行记录的数据库表对象；如没有，则返回{@code null}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> T get(T basePo) throws DataAccessException;

	/**
	 * 按条件查询PO
	 * <p>
	 * 查询范围通过{@code where}进行指定
	 * </p>
	 *
	 * <pre>
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     XxPo xxPo = get(new XxPo(), "name = ?", params);
	 *     XxPo xxPo = get(new XxPo(), "name = ?", "王五");
	 *     XxPo xxPo = get(new XxPo(), "name = ?", new Object[] { "王五" });
	 * </pre>
	 * @param basePo PO对象
	 * @param where {@code where}条件，如status = ?
	 * @param params {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 如where条件对应的有记录，则返回该行记录的数据库表对象；如没有，则返回{@code null}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> T get(T basePo, String where, Object... params)
			throws DataAccessException;

	/**
	 * 按条件查询单PO
	 * <p>
	 * 查询范围通过{@code where}进行指定
	 * </p>
	 *
	 * <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     XxPo xxPo = get(new XxPo(), "name = ?", params);
	 * </pre>
	 * @param basePo PO对象
	 * @param where {@code where}条件，如status = :status
	 * @param params {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 如where条件对应的有记录，则返回该行记录的数据库表对象；如没有，则返回{@code null}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> T get(T basePo, String where, Map<String, Object> params)
			throws DataAccessException;

	/**
	 * 按条件查询单PO
	 * <p>
	 * 查询范围通过{@code where}进行指定
	 * </p>
	 *
	 * <pre>
	 *    {@code MapSqlParameterSource params = new MapSqlParameterSource();}
	 *     params.addValue("name", "王五");
	 *
	 *     XxPo xxPo = get(new XxPo(), "name = ?", params);
	 * </pre>
	 * @param basePo PO对象
	 * @param where {@code where}条件，如status = :status
	 * @param params {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 如where条件对应的有记录，则返回该行记录的数据库表对象；如没有，则返回{@code null}
	 * @throws DataAccessException 主键为空、参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	<T extends BasePo> T get(T basePo, String where, SqlParameterSource params)
			throws DataAccessException;

	/**
	 * 查询Map
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select * from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code Map<String, Object> xxMap = get(sql, params)}
	 *    {@code Map<String, Object> xxMap = get(sql, "王五")}
	 *    {@code Map<String, Object> xxMap = get(sql, new Object[] { "王五" })}
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	Map<String, Object> get(String sql, Object... params) throws DataAccessException;

	/**
	 * 查询Map
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select * from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *    {@code Map<String, Object> xxMap = get(sql, params)}
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	Map<String, Object> get(String sql, Map<String, Object> params)
			throws DataAccessException;

	/**
	 * 查询Map
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select * from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *    {@code Map<String, Object> xxMap = get(sql, params)}
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	Map<String, Object> get(String sql, SqlParameterSource params)
			throws DataAccessException;

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     String xx = get(sql, String.class, params);
	 *     String xx = get(sql, String.class, "王五")
	 *     String xx = get(sql, String.class, new Object[] { "王五" })
	 *     Long xx = get(sql, params, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param requiredType 指定值类型的Class
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	<T> T get(String sql, Class<T> requiredType, Object... params)
			throws DataAccessException;

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     String xx = get(sql, params, String.class);
	 *     String xx = get(sql, new Object[] { "王五" }, String.class)
	 *     Long xx = get(sql, params, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	<T> T get(String sql, Object[] params, Class<T> requiredType)
			throws DataAccessException;

	/**
	 * 查询class对象
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     String xx = get(sql, params, String.class);
	 *     Long xx = get(sql, params, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	<T> T get(String sql, Map<String, Object> params, Class<T> requiredType)
			throws DataAccessException;

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     String xx = get(sql, params, String.class);
	 *     Long xx = get(sql, params, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	<T> T get(String sql, SqlParameterSource params, Class<T> requiredType)
			throws DataAccessException;

	/**
	 * 查询指定类型的对象
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code RowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code String xx = get(sql, new SingleColumnRowMapper<>(String.class), params)};
	 *    {@code String xx = get(sql, new SingleColumnRowMapper<>(String.class), "王五")};
	 *    {@code String xx = get(sql, new SingleColumnRowMapper<>(String.class), new Object[] { "王五" })};
	 *    {@code Long xx = get(sql, new SingleColumnRowMapper<>(Long.class), "王五")};
	 *    {@code Long xx = get(sql, new SingleColumnRowMapper<>(Long.class), params)};
	 *    {@code XxPo xxPo = get(sql, new BeanPropertyRowMapper<>(XxPo.class), "王五"};
	 *    {@code XxPo xxPo = get(sql, new BeanPropertyRowMapper<>(XxPo.class), params};
	 *    {@code Map<String, Object> xxMap = get(sql, new ColumnMapRowMapper(), "王五"};
	 *    {@code Map<String, Object> xxMap = get(sql, new ColumnMapRowMapper(), params};
	 * </pre>
	 * @param sql sql语句
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> T get(String sql, RowMapper<T> rowMapper, Object... params)
			throws DataAccessException;

	/**
	 * 查询指定类型的对象
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code RowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code String xx = get(sql, params, new SingleColumnRowMapper<>(String.class))};
	 *    {@code String xx = get(sql, new Object[] { "王五" }, new SingleColumnRowMapper<>(String.class))};
	 *    {@code Long xx = get(sql, params, new SingleColumnRowMapper<>(Long.class))};
	 *    {@code XxPo xxPo = get(sql, params, new BeanPropertyRowMapper<>(XxPo.class)};
	 *    {@code Map<String, Object> xxMap = get(sql, params, new ColumnMapRowMapper()};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> T get(String sql, Object[] params, RowMapper<T> rowMapper)
			throws DataAccessException;

	/**
	 * 查询指定类型的对象
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code RowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *    {@code String xx = get(sql, params, new SingleColumnRowMapper<>(String.class))};
	 *    {@code Long xx = get(sql, params, new SingleColumnRowMapper<>(Long.class))};
	 *    {@code XxPo xxPo = get(sql, params, new BeanPropertyRowMapper<>(XxPo.class)};
	 *    {@code Map<String, Object> xxMap = get(sql, params, new ColumnMapRowMapper()};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> T get(String sql, Map<String, Object> params, RowMapper<T> rowMapper)
			throws DataAccessException;

	/**
	 * 查询指定类型的对象
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回对象类型通过{@code RowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *    {@code String xx = get(sql, params, new SingleColumnRowMapper<>(String.class))};
	 *    {@code Long xx = get(sql, params, new SingleColumnRowMapper<>(Long.class))};
	 *    {@code XxPo xxPo = get(sql, params, new BeanPropertyRowMapper<>(XxPo.class)};
	 *    {@code Map<String, Object> xxMap = get(sql, params, new ColumnMapRowMapper()};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以Map形式返回该记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> T get(String sql, SqlParameterSource params, RowMapper<T> rowMapper)
			throws DataAccessException;

	/**
	 * 按条件查询PO列表 <pre>
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", params)};
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", "王五")};
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", new Object[] { "王五" })};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 */
	<T extends BasePo> List<T> select(T basePo, String where, Object... params)
			throws DataAccessException;

	/**
	 * 按条件查询PO列表 <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = :name", params)};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 */
	<T extends BasePo> List<T> select(T basePo, String where, Map<String, Object> params)
			throws DataAccessException;

	/**
	 * 按条件查询PO列表 <pre>
	 *    {@code MapSqlParameterSource params = new MapSqlParameterSource();}
	 *     params.addValue("name", "王五");
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = :name", params)};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 */
	<T extends BasePo> List<T> select(T basePo, String where, SqlParameterSource params)
			throws DataAccessException;

	/**
	 * 按条件分页查询PO列表 <pre>
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", params, pageInfo)};
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", new Object[] { "王五" }, pageInfo)};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param pageInfo 分页信息
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足where及分页条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 * @see PageInfo
	 */
	<T extends BasePo> List<T> select(T basePo, String where, PageInfo pageInfo,
			Object... params) throws DataAccessException;

	/**
	 * 按条件分页查询PO列表 <pre>
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", params, pageInfo)};
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = ?", new Object[] { "王五" }, pageInfo)};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足where及分页条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 * @see PageInfo
	 */
	<T extends BasePo> List<T> select(T basePo, String where, Object[] params,
			PageInfo pageInfo) throws DataAccessException;

	/**
	 * 按条件分页查询PO列表 <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = :name", params, pageInfo)};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足where及分页条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 * @see PageInfo
	 */
	<T extends BasePo> List<T> select(T basePo, String where, Map<String, Object> params,
			PageInfo pageInfo) throws DataAccessException;

	/**
	 * 按条件分页查询PO列表 <pre>
	 *    {@code MapSqlParameterSource params = new MapSqlParameterSource();}
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(new XxPo(), "name = :name", params, pageInfo)};
	 * </pre>
	 * @param basePo PO对象
	 * @param where sql {@code where}条件，形如{@code " status = :statis"}
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param <T> BasePo的子类，即数据库表对象
	 * @return 满足where及分页条件的PO对象列表
	 * @throws DataAccessException 参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see BasePo
	 * @see PageInfo
	 */
	<T extends BasePo> List<T> select(T basePo, String where, SqlParameterSource params,
			PageInfo pageInfo) throws DataAccessException;

	/**
	 * 查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code List<String> xxs = select(sql, String.class, params)};
	 *    {@code List<String> xxs = select(sql, String.class, "王五")};
	 *    {@code List<String> xxs = select(sql, String.class, new Object[] { "王五" })};
	 *    {@code List<Long> xxs = select(sql, params, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param requiredType 指定值类型的Class
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	<T> List<T> select(String sql, Class<T> requiredType, Object... params)
			throws DataAccessException;

	/**
	 * 查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code List<String> xxs = select(sql, params, String.class)};
	 *    {@code List<String> xxs = select(sql, new Object[] { "王五" }, String.class)};
	 *    {@code List<Long> xxs = select(sql, params, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	<T> List<T> select(String sql, Object[] params, Class<T> requiredType)
			throws DataAccessException;

	/**
	 * 查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *    {@code List<String> xxs = select(sql, params, String.class)};
	 *    {@code List<Long> xxs = select(sql, params, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	<T> List<T> select(String sql, Map<String, Object> params, Class<T> requiredType)
			throws DataAccessException;

	/**
	 * 查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *    {@code List<String> xxs = select(sql, params, String.class)};
	 *    {@code List<Long> xxs = select(sql, params, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	<T> List<T> select(String sql, SqlParameterSource params, Class<T> requiredType)
			throws DataAccessException;

	/**
	 * 分页查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<String> xxs = select(sql, pageInfo, String.class, params)};
	 *    {@code List<String> xxs = select(sql, pageInfo, String.class, "王五")};
	 *    {@code List<String> xxs = select(sql, pageInfo, String.class, new Object[] { "王五" })};
	 *    {@code List<Long> xxs = select(sql, pageInfo, Long.class, "王五")};
	 *    {@code List<Long> xxs = select(sql, pageInfo, Long.class, params)};
	 * </pre>
	 * @param sql sql语句
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	<T> List<T> select(String sql, PageInfo pageInfo, Class<T> requiredType,
			Object... params) throws DataAccessException;

	/**
	 * 分页查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<String> xxs = select(sql, params, pageInfo, String.class)};
	 *    {@code List<String> xxs = select(sql, new Object[] { "王五" }, pageInfo, String.class)};
	 *    {@code List<Long> xxs = select(sql, params, pageInfo, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	<T> List<T> select(String sql, Object[] params, PageInfo pageInfo,
			Class<T> requiredType) throws DataAccessException;

	/**
	 * 分页查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<String> xxs = select(sql, params, pageInfo, String.class)};
	 *    {@code List<Long> xxs = select(sql, params, pageInfo, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	<T> List<T> select(String sql, Map<String, Object> params, PageInfo pageInfo,
			Class<T> requiredType) throws DataAccessException;

	/**
	 * 分页查询指定类型的值列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<String> xxs = select(sql, params, pageInfo, String.class)};
	 *    {@code List<Long> xxs = select(sql, params, pageInfo, Long.class)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	<T> List<T> select(String sql, SqlParameterSource params, PageInfo pageInfo,
			Class<T> requiredType) throws DataAccessException;

	/**
	 * 查询指定类型的值列表的分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = pageInfo(sql, pageInfo, String.class, params);
	 *     PageInfo pageInfo = pageInfo(sql, pageInfo, String.class, "王五");
	 *     PageInfo pageInfo = pageInfo(sql, pageInfo, String.class, new Object[] { "王五" });
	 *     PageInfo pageInfo = pageInfo(sql, pageInfo, Long.class, params);
	 * </pre>
	 * @param sql sql语句
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录的分页信息；如查询不到，则返回{@code result}为空的{@code pageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	<T> PageInfo pageInfo(String sql, PageInfo pageInfo, Class<T> requiredType,
			Object... params) throws DataAccessException;

	/**
	 * 查询指定类型的值列表的分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = pageInfo(sql, params, pageInfo, String.class);
	 *     PageInfo pageInfo = pageInfo(sql, new Object[] { "王五" }, pageInfo, String.class);
	 *     PageInfo pageInfo = pageInfo(sql, params, pageInfo, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录的分页信息；如查询不到，则返回{@code result}为空的{@code pageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	<T> PageInfo pageInfo(String sql, Object[] params, PageInfo pageInfo,
			Class<T> requiredType) throws DataAccessException;

	/**
	 * 查询指定类型的值列表的分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = pageInfo(sql, params, pageInfo, String.class);
	 *     PageInfo pageInfo = pageInfo(sql, params, pageInfo, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录的分页信息；如查询不到，则返回{@code result}为空的{@code pageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	<T> PageInfo pageInfo(String sql, Map<String, Object> params, PageInfo pageInfo,
			Class<T> requiredType) throws DataAccessException;

	/**
	 * 查询指定类型的值列表的分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code requiredType}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，{@code requiredType}不能是PO、VO、DTO或自定义Bean等的JavaBean。
	 * 正确的类型为java.lang.Long、java.lang.Integer、java.lang.Double等
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = pageInfo(sql, params, pageInfo, String.class);
	 *     PageInfo pageInfo = pageInfo(sql, params, pageInfo, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param pageInfo 分页信息
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录的分页信息；如查询不到，则返回{@code result}为空的{@code pageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 * @see org.springframework.jdbc.core.namedparam.EmptySqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.MapSqlParameterSource
	 */
	<T> PageInfo pageInfo(String sql, SqlParameterSource params, PageInfo pageInfo,
			Class<T> requiredType) throws DataAccessException;

	/**
	 * 查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code List<XxPo> xxPos = select(sql, BeanPropertyRowMapper.newInstance(XxPo.class), params)};
	 *    {@code List<String> xxs = select(sql, SingleColumnRowMapper.newInstance(String.class), "王五")};
	 *    {@code List<String> xxs = select(sql, SingleColumnRowMapper.newInstance(String.class), new Object[] { "王五" })};
	 *    {@code List<Map<String, Object>> xxs = select(sql, new ColumnMapRowMapper(), "王五")};
	 *    {@code List<Map<String, Object>> xxs = select(sql, new ColumnMapRowMapper(), params)};
	 * </pre>
	 * @param sql sql语句
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> List<T> select(String sql, RowMapper<T> rowMapper, Object... params)
			throws DataAccessException;

	/**
	 * 查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code List<XxPo> xxPos = select(sql, params, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *    {@code List<String> xxs = select(sql, new Object[] { "王五" }, 为SingleColumnRowMapper.newInstance(String.class))};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> List<T> select(String sql, Object[] params, RowMapper<T> rowMapper)
			throws DataAccessException;

	/**
	 * 查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *    {@code List<XxPo> xxPos = select(sql, params, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *    {@code List<String> xxs = select(sql, params, 为SingleColumnRowMapper.newInstance(String.class))};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> List<T> select(String sql, Map<String, Object> params, RowMapper<T> rowMapper)
			throws DataAccessException;

	/**
	 * 查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *    {@code List<XxPo> xxPos = select(sql, params, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *    {@code List<String> xxs = select(sql, params, 为SingleColumnRowMapper.newInstance(String.class))};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> List<T> select(String sql, SqlParameterSource params, RowMapper<T> rowMapper)
			throws DataAccessException;

	/**
	 * 分页查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(sql, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class), params)};
	 *    {@code List<String> xxs = select(sql, pageInfo, SingleColumnRowMapper.newInstance(String.class), "王五")};
	 *    {@code List<String> xxs = select(sql, pageInfo, SingleColumnRowMapper.newInstance(String.class), new Object[] { "王五" })};
	 *    {@code List<Map<String, Object>> xxs = select(sql, pageInfo, new ColumnMapRowMapper(), "王五")};
	 *    {@code List<Map<String, Object>> xxs = select(sql, pageInfo, new ColumnMapRowMapper(), params)};
	 * </pre>
	 * @param sql sql语句
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> List<T> select(String sql, PageInfo pageInfo, RowMapper<T> rowMapper,
			Object... params) throws DataAccessException;

	/**
	 * 分页查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(sql, params, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *    {@code List<String> xxs = select(sql, new Object[] { "王五" }, pageInfo, SingleColumnRowMapper.newInstance(String.class))};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, pageInfo, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> List<T> select(String sql, Object[] params, PageInfo pageInfo,
			RowMapper<T> rowMapper) throws DataAccessException;

	/**
	 * 分页查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(sql, params, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *    {@code List<String> xxs = select(sql, params, pageInfo, SingleColumnRowMapper.newInstance(String.class))};
	 *    {@code List<Map<String, Object>> xxs = get(sql, params, pageInfo, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> List<T> select(String sql, Map<String, Object> params, PageInfo pageInfo,
			RowMapper<T> rowMapper) throws DataAccessException;

	/**
	 * 分页查询指定类型的对象列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<XxPo> xxPos = select(sql, params, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *    {@code List<String> xxs = select(sql, params, pageInfo, SingleColumnRowMapper.newInstance(String.class))};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, pageInfo, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> List<T> select(String sql, SqlParameterSource params, PageInfo pageInfo,
			RowMapper<T> rowMapper) throws DataAccessException;

	/**
	 * 查询指定类型的对象列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class), params)};
	 *     PageInfo pageInfo = select(sql, pageInfo, SingleColumnRowMapper.newInstance(String.class), "王五")};
	 *     PageInfo pageInfo = select(sql, pageInfo, SingleColumnRowMapper.newInstance(String.class), new Object[] { "王五" })};
	 *     PageInfo pageInfo = select(sql, pageInfo, new ColumnMapRowMapper(), "王五")};
	 *     PageInfo pageInfo = select(sql, pageInfo, new ColumnMapRowMapper(), params)};
	 * </pre>
	 * @param sql sql语句
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql及分页能查询到记录，则以集合形式存储在{@code PageInfo的list}中；如查询不到，则返回{@code list}为null的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> PageInfo pageInfo(String sql, PageInfo pageInfo, RowMapper<T> rowMapper,
			Object... params) throws DataAccessException;

	/**
	 * 查询指定类型的对象列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, params, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *     PageInfo pageInfo = select(sql, new Object[] { "王五" }, pageInfo, SingleColumnRowMapper.newInstance(String.class))};
	 *     PageInfo pageInfo = select(sql, params, pageInfo, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql及分页能查询到记录，则以集合形式存储在{@code PageInfo的list}中；如查询不到，则返回{@code list}为null的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> PageInfo pageInfo(String sql, Object[] params, PageInfo pageInfo,
			RowMapper<T> rowMapper) throws DataAccessException;

	/**
	 * 查询指定类型的对象列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, params, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class))};
	 *     PageInfo pageInfo = select(sql, params, pageInfo, SingleColumnRowMapper.newInstance(String.class))};
	 *     PageInfo pageInfo = get(sql, params, pageInfo, new ColumnMapRowMapper())};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql及分页能查询到记录，则以集合形式存储在{@code PageInfo的list}中；如查询不到，则返回{@code list}为null的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> PageInfo pageInfo(String sql, Map<String, Object> params, PageInfo pageInfo,
			RowMapper<T> rowMapper) throws DataAccessException;

	/**
	 * 查询指定类型的对象列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, params, pageInfo, BeanPropertyRowMapper.newInstance(XxPo.class));
	 *     PageInfo pageInfo = select(sql, params, pageInfo, SingleColumnRowMapper.newInstance(String.class));
	 *     PageInfo pageInfo xxs = select(sql, params, pageInfo, new ColumnMapRowMapper());
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 如该sql及分页能查询到记录，则以集合形式存储在{@code PageInfo的list}中；如查询不到，则返回{@code list}为null的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.SingleColumnRowMapper
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper
	 * @see org.springframework.jdbc.core.ColumnMapRowMapper
	 */
	<T> PageInfo pageInfo(String sql, SqlParameterSource params, PageInfo pageInfo,
			RowMapper<T> rowMapper) throws DataAccessException;

	/**
	 * 查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, params)};
	 *    {@code List<Map<String, Object>> xxs = select(sql, "王五")};
	 *    {@code List<Map<String, Object>> xxs = select(sql, new Object[] { "王五" })};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	List<Map<String, Object>> select(String sql, Object... params)
			throws DataAccessException;

	/**
	 * 查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, params)};
	 *    {@code List<Map<String, Object>> xxs = select(sql, new Object[] { "王五" })};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	List<Map<String, Object>> select(String sql, Map<String, Object> params)
			throws DataAccessException;

	/**
	 * 查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, params)};
	 *    {@code List<Map<String, Object>> xxs = select(sql, new Object[] { "王五" })};
	 *    {@code List<Map<String, Object>> xxs = select(sql, params};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.MapSqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.EmptySqlParameterSource
	 */
	List<Map<String, Object>> select(String sql, SqlParameterSource params)
			throws DataAccessException;

	/**
	 * 分页查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, pageInfo, params)};
	 *    {@code List<Map<String, Object>> xxs = select(sql, pageInfo, "王五")};
	 *    {@code List<Map<String, Object>> xxs = select(sql, pageInfo, new Object[] { "王五" })};
	 * </pre>
	 * @param sql sql语句
	 * @param pageInfo 分页信息
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	List<Map<String, Object>> select(String sql, PageInfo pageInfo, Object... params)
			throws DataAccessException;

	/**
	 * 分页查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, pageInfo)};
	 *    {@code List<Map<String, Object>> xxs = select(sql, new Object[] { "王五" }, pageInfo)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	List<Map<String, Object>> select(String sql, Object[] params, PageInfo pageInfo)
			throws DataAccessException;

	/**
	 * 分页查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, pageInfo)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	List<Map<String, Object>> select(String sql, Map<String, Object> params,
			PageInfo pageInfo) throws DataAccessException;

	/**
	 * 分页查询Map列表
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *    {@code List<Map<String, Object>> xxs = select(sql, params, pageInfo)};
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 * @see org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.MapSqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.EmptySqlParameterSource
	 */
	List<Map<String, Object>> select(String sql, SqlParameterSource params,
			PageInfo pageInfo) throws DataAccessException;

	/**
	 * 查询Map列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, pageInfo, params);
	 *     PageInfo pageInfo = select(sql, pageInfo, "王五");
	 *     PageInfo pageInfo = select(sql, pageInfo, new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param pageInfo 分页信息
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code result}为空的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	PageInfo pageInfo(String sql, PageInfo pageInfo, Object... params)
			throws DataAccessException;

	/**
	 * 查询Map列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = ?";
	 *
	 *     Object[] params = new Object[1];
	 *     params[0] = "王五";
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, params, pageInfo);
	 *     PageInfo pageInfo = select(sql, new Object[] { "王五" }, pageInfo);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @return 如该sql能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code result}为空的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、或者执行过程中发生的其它异常
	 */
	PageInfo pageInfo(String sql, Object[] params, PageInfo pageInfo)
			throws DataAccessException;

	/**
	 * 查询Map列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, params, pageInfo);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code result}为空的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 */
	PageInfo pageInfo(String sql, Map<String, Object> params, PageInfo pageInfo)
			throws DataAccessException;

	/**
	 * 查询Map列表分页信息
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定
	 * </p>
	 * <pre>
	 *     String sql = "select xx from xx where name = :name";
	 *
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     PageInfo pageInfo = new PageInfo();
	 *     // 可设置每页大小，默认为10
	 *     pageInfo.setPageSize(5);
	 *
	 *     PageInfo pageInfo = select(sql, params, pageInfo);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param pageInfo 分页信息
	 * @return 如该sql及分页能查询到记录，则以集合形式返回这些记录；如查询不到，则返回{@code result}为空的{@code PageInfo}
	 * @throws DataAccessException sql不正确、参数与where不匹配、分页异常、或者执行过程中发生的其它异常
	 *
	 * @see PageInfo
	 * @see org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.MapSqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.EmptySqlParameterSource
	 */
	PageInfo pageInfo(String sql, SqlParameterSource params, PageInfo pageInfo)
			throws DataAccessException;

	/**
	 * 查询int <pre>
	 *     int age = queryForInt("select age from user where name = ?", "王五");
	 *     int age = queryForInt("select age from user where name = ?", new Object[] { "王五" });
	 *     int count = queryForInt("select count(*) from user where name = ?", new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的int值，默认为{@code 0}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Integer类型，会抛出此异常
	 */
	int queryForInt(String sql, Object... params) throws DataAccessException;

	/**
	 * 查询int <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     int age = queryForInt("select age from user where name = :name", params);
	 *     int count = queryForInt("select count(*) from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的int值，默认为{@code 0}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Integer类型，会抛出此异常
	 */
	int queryForInt(String sql, Map<String, Object> params) throws DataAccessException;

	/**
	 * 查询int <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     int age = queryForInt("select age from user where name = :name", params);
	 *     int count = queryForInt("select count(*) from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的int值，默认为{@code 0}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Integer类型，会抛出此异常
	 */
	int queryForInt(String sql, SqlParameterSource params) throws DataAccessException;

	/**
	 * 查询Long <pre>
	 *     Long amount = queryForLong("select amount from user where name = ?", "王五");
	 *     Long amount = queryForLong("select amount from user where name = ?", new Object[] { "王五" });
	 *     Long count = queryForLong("select count(*) from user where name = ?", new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的Long值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Long类型，会抛出此异常
	 */
	Long queryForLong(String sql, Object... params) throws DataAccessException;

	/**
	 * 查询Long <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     Long amount = queryForLong("select amount from user where name = :name", params);
	 *     Long count = queryForLong("select count(*) from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的Long值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Long类型，会抛出此异常
	 */
	Long queryForLong(String sql, Map<String, Object> params) throws DataAccessException;

	/**
	 * 查询Long <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     Long amount = queryForLong("select amount from user where name = :name", params);
	 *     Long count = queryForLong("select count(*) from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的Long值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Long类型，会抛出此异常
	 */
	Long queryForLong(String sql, SqlParameterSource params) throws DataAccessException;

	/**
	 * 查询Double <pre>
	 *     Double amount = queryForDouble("select amount from user where name = ?", "王五");
	 *     Double amount = queryForDouble("select amount from user where name = ?", new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的Double值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Double类型，会抛出此异常
	 */
	Double queryForDouble(String sql, Object... params) throws DataAccessException;

	/**
	 * 查询Double <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     Double amount = queryForDouble("select amount from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的Double值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Double类型，会抛出此异常
	 */
	Double queryForDouble(String sql, Map<String, Object> params)
			throws DataAccessException;

	/**
	 * 查询Double <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     Double amount = queryForDouble("select amount from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的Double值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为Double类型，会抛出此异常
	 */
	Double queryForDouble(String sql, SqlParameterSource params)
			throws DataAccessException;

	/**
	 * 查询BigDecimal <pre>
	 *     BigDecimal amount = queryForBigDecimal("select amount from user where name = ?", "王五");
	 *     BigDecimal amount = queryForBigDecimal("select amount from user where name = ?", new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的BigDecimal值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为BigDecimal类型，会抛出此异常
	 */
	BigDecimal queryForBigDecimal(String sql, Object... params)
			throws DataAccessException;

	/**
	 * 查询BigDecimal <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     BigDecimal amount = queryForBigDecimal("select amount from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的BigDecimal值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为BigDecimal类型，会抛出此异常
	 */
	BigDecimal queryForBigDecimal(String sql, Map<String, Object> params)
			throws DataAccessException;

	/**
	 * 查询BigDecimal <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     BigDecimal amount = queryForBigDecimal("select amount from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的BigDecimal值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型转换异常、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为BigDecimal类型，会抛出此异常
	 */
	BigDecimal queryForBigDecimal(String sql, SqlParameterSource params)
			throws DataAccessException;

	/**
	 * 查询String <pre>
	 *     String xx = queryForString("select xx from user where name = ?", "王五");
	 *     String xx = queryForString("select xx from user where name = ?", new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的String值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为String类型，会抛出此异常
	 */
	String queryForString(String sql, Object... params) throws DataAccessException;

	/**
	 * 查询String <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     String xx = queryForString("select xx from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的String值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为String类型，会抛出此异常
	 */
	String queryForString(String sql, Map<String, Object> params)
			throws DataAccessException;

	/**
	 * 查询String <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     String xx = queryForString("select xx from user where name = :name", params);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @return 查询结果的String值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为String类型，会抛出此异常
	 */
	String queryForString(String sql, SqlParameterSource params)
			throws DataAccessException;

	/**
	 * 查询指定类型的值 <pre>
	 *     String xx = queryForObject("select xx from user where name = ?", String.class, "王五");
	 *     String xx = queryForObject("select xx from user where name = ?", String.class, new Object[] { "王五" });
	 *     Long xx = queryForObject("select xx from user where name = ?", Long.class, new Object[] { "王五" });
	 * </pre>
	 * @param sql sql语句
	 * @param requiredType 指定值类型的Class
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	<T> T queryForObject(String sql, Class<T> requiredType, Object... params)
			throws DataAccessException;

	/**
	 * 查询指定类型的值 <pre>
	 *     String xx = queryForObject("select xx from user where name = ?", new Object[] { "王五" }, String.class);
	 *     Long xx = queryForObject("select xx from user where name = ?", new Object[] { "王五" }, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	<T> T queryForObject(String sql, Object[] params, Class<T> requiredType)
			throws DataAccessException;

	/**
	 * 查询指定类型的值 <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     String xx = queryForObject("select xx from user where name = :name", params, String.class);
	 *     Long xx = queryForObject("select xx from user where name = :name", params, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	<T> T queryForObject(String sql, Map<String, Object> params, Class<T> requiredType)
			throws DataAccessException;

	/**
	 * 查询指定类型的值 <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     String xx = queryForObject("select xx from user where name = :name", params, String.class);
	 *     Long xx = queryForObject("select xx from user where name = :name", params, Long.class);
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param requiredType 指定值类型的Class
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	<T> T queryForObject(String sql, SqlParameterSource params, Class<T> requiredType)
			throws DataAccessException;

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String xx = queryForObject("select xx from user where name = ?", new SingleColumnRowMapper(String.class), "王五");
	 *     String xx = queryForObject("select xx from user where name = ?", new SingleColumnRowMapper(String.class), new Object[] { "王五" });
	 *     Long xx = queryForObject("select xx from user where name = ?", new SingleColumnRowMapper(Long.class), new Object[] { "王五" });
	 *     XxPo xxPo = queryForObject("select xx from user where name = ?", BeanPropertyRowMapper.newInstance(XxPo.class), "王五");
	 * </pre>
	 * @param sql sql语句
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param params sql {@code where}条件中参数对应的值
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	<T> T queryForObject(String sql, RowMapper<T> rowMapper, Object... params)
			throws DataAccessException;

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     String xx = queryForObject("select xx from user where name = ?", new Object[] { "王五" }, new SingleColumnRowMapper(String.class));
	 *     Long xx = queryForObject("select xx from user where name = ?", new Object[] { "王五" }, new SingleColumnRowMapper(Long.class));
	 *     XxPo xxPo = queryForObject("select xx from user where name = ?", new Object[] { "王五" }, BeanPropertyRowMapper.newInstance(XxPo.class));
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	<T> T queryForObject(String sql, Object[] params, RowMapper<T> rowMapper)
			throws DataAccessException;

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("name", "王五");
	 *
	 *     String xx = queryForObject("select xx from user where name = :name", params, new SingleColumnRowMapper(String.class));
	 *     Long xx = queryForObject("select xx from user where name = :name", params, new SingleColumnRowMapper(Long.class));
	 *     XxPo xxPo = queryForObject("select xx from user where name = :name", params, BeanPropertyRowMapper.newInstance(XxPo.class));
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @param <T> 指定值类型，如java.lang.Long、java.lang.Integer、java.lang.Double
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	<T> T queryForObject(String sql, Map<String, Object> params, RowMapper<T> rowMapper)
			throws DataAccessException;

	/**
	 * 查询指定类型的值
	 * <p>
	 * 查询内容通过{@code sql}指定，查询参数通过{@code params}指定，返回值类型通过{@code rowMapper}指定
	 * </p>
	 * <p>
	 * 需要特别注意的是，如需返回JavaBean类型的对象，需指定{@code RowMapper}为BeanPropertyRowMapper(class)，此时class代表预期返回的JavaBean类型;
	 * 如需返回单列值对象，需指定{@code RowMapper}为SingleColumnRowMapper(class)，此时class代表预期返回的值的类型，如String、Long等;
	 * 如需返回Map对象，需{@code RowMapper}为ColumnMapRowMapper。其中，如需返回JavaBean类型的对象中的属性名称必须与数据库表中的字段名保持一致。
	 * </p>
	 * <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("name", "王五");
	 *
	 *     String xx = queryForObject("select xx from user where name = :name", params, new SingleColumnRowMapper(String.class));
	 *     Long xx = queryForObject("select xx from user where name = :name", params, new SingleColumnRowMapper(Long.class));
	 *     XxPo xxPo = queryForObject("select xx from user where name = :name", params, BeanPropertyRowMapper.newInstance(XxPo.class));
	 * </pre>
	 * @param sql sql语句
	 * @param params sql {@code where}条件中参数对应的值
	 * @param rowMapper 指定返回对象类型的RowMapper
	 * @param <T> 指定对象类型，如XxPo、XxBean、XxVo、String、Long
	 * @return 查询结果的指定类型值，默认为{@code null}
	 * @throws DataAccessException sql不正确、参数与where不匹配、类型不匹配、执行过程中的其它异常
	 * @exception org.springframework.dao.EmptyResultDataAccessException 如果查询不到记录，会抛出此异常
	 * @exception org.springframework.dao.IncorrectResultSizeDataAccessException
	 * 如果查询到大于1条的记录，会抛出此异常
	 * @exception org.springframework.dao.TypeMismatchDataAccessException
	 * 如查询到的值不能转换为指定的{@code T}类型，会抛出此异常
	 */
	<T> T queryForObject(String sql, SqlParameterSource params, RowMapper<T> rowMapper)
			throws DataAccessException;

	/**
	 * 数据库更新操作
	 *
	 * <p>
	 * {@code insert}、{@code update}、{@code delete}操作均可调用此方法。
	 * </p>
	 * <pre>
	 *     update("insert into tableName (colum) values (?)"), new Object[] { "1122"})
	 *     update("update tableName set colum = :colum where colum = ?"), "1122")
	 *     update("delete from tableName where colum = ?"), new Object[] { "1122"})
	 * </pre>
	 * @param sql sql语句
	 * @param params 参数列表
	 * @return 执行成功行数，如全部失败，则返回{@code 0}
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	int update(String sql, Object... params) throws DataAccessException;

	/**
	 * 数据库更新操作
	 *
	 * <p>
	 * {@code insert}、{@code update}、{@code delete}操作均可调用此方法。
	 * </p>
	 * <pre>
	 *    {@code Map<String, Object> params = new HashMap<>();}
	 *     params.put("colum", "1122");
	 *
	 *     update("insert into tableName (colum) values (:colum)"), params)
	 *     update("update tableName set colum = :colum where colum = :colum"), params)
	 *     update("delete from tableName where colum = :colum"), params)
	 * </pre>
	 * @param sql sql语句
	 * @param params 参数列表
	 * @return 执行成功行数，如全部失败，则返回{@code 0}
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 */
	int update(String sql, Map<String, Object> params) throws DataAccessException;

	/**
	 * 数据库更新操作
	 *
	 * <p>
	 * {@code insert}、{@code update}、{@code delete}操作均可调用此方法。
	 * </p>
	 * <pre>
	 *     MapSqlParameterSource params = new MapSqlParameterSource();
	 *     params.addValue("colum", "1122");
	 *
	 *     update("insert into tableName (colum) values (:colum)"), params)
	 *     update("update tableName set colum = :colum where colum = :colum"), params)
	 *     update("delete from tableName where colum = :colum"), params)
	 * </pre>
	 * @param sql sql语句
	 * @param params 参数列表
	 * @return 执行成功行数，如全部失败，则返回{@code 0}
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.namedparam.EmptySqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.MapSqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
	 */
	int update(String sql, SqlParameterSource params) throws DataAccessException;

	/**
	 * 批量数据库更新操作
	 *
	 * <p>
	 * {@code insert}、{@code update}、{@code delete}操作均可调用此方法。
	 * </p>
	 * <pre>
	 *     // {@code List<Object[]>}方式
	 *     Object[] params_1 = new Object[] { "王五" };
	 *     Object[] params_2 = new Object[] { "李四" };
	 *
	 *     {@code List<Object[]> params = new ArrayList<>()};
	 *     params.add(params_1);
	 *     params.add(params_2);
	 *
	 *     // SqlParameterSource的MapSqlParameterSource方式
	 *     MapSqlParameterSource params_1 = new MapSqlParameterSource();
	 *     params.addValue("colum", "1122");
	 *
	 *     MapSqlParameterSource params_2 = new MapSqlParameterSource();
	 *     params.addValue("colum", "3344");
	 *
	 *     {@code List<MapSqlParameterSource> params = new ArrayList<>()};
	 *     params.add(params_1);
	 *     params.add(params_2);
	 *
	 *     XxPo xxPo_1 = new XxPo();
	 *     xxPo_1.setName("王五");
	 *
	 *     XxPo xxPo_2 = new XxPo();
	 *     xxPo_2.setName("李四");
	 *
	 *     // SqlParameterSource的BeanPropertySqlParameterSource方式
	 *     BeanPropertySqlParameterSource params_1 = new BeanPropertySqlParameterSource(xxPo_1);
	 *     BeanPropertySqlParameterSource params_2 = new BeanPropertySqlParameterSource(xxPo_2);
	 *
	 *     {@code List<BeanPropertySqlParameterSource> params = new ArrayList<>()};
	 *     params.add(params_1);
	 *     params.add(params_2);
	
	 *
	 *     update("insert into tableName (colum) values (:colum)"), params);
	 *     update("update tableName set colum = :colum where colum = :colum"), params);
	 *     update("delete from tableName where colum = :colum"), params);
	 *
	 *
	 * </pre>
	 * @param sql sql语句
	 * @param params 参数列表
	 * @return 执行成功行数，如全部失败，则返回{@code 0}
	 * @throws DataAccessException 参数异常、api方法调用异常、或者执行过程中发生的其它异常
	 *
	 * @see org.springframework.jdbc.core.namedparam.EmptySqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.MapSqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
	 * @see org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils
	 */
	int batchUpdate(String sql, List<?> params) throws DataAccessException;

}
