package org.xbdframework.dao;

import java.math.BigDecimal;
import java.util.*;

import com.alibaba.fastjson.JSONObject;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import org.xbdframework.dao.core.BasePo;
import org.xbdframework.dao.core.GG_CZYZY;
import org.xbdframework.dao.core.PageInfo;
import org.xbdframework.dao.core.TestUserPo;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-*.xml")
@WebAppConfiguration
public class BaseDaoTest {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private BaseDao dao;

	@Test
	public void insert() {
		TestUserPo testUserPo = new TestUserPo();
		testUserPo.setId(UUID.randomUUID().toString().replace("-", ""));
		testUserPo.setName("王五");
		testUserPo.setAge(20);

		assertTrue(this.dao.insert(testUserPo) == 1);

		GG_CZYZY gg_czyzy = new GG_CZYZY();
		gg_czyzy.setId(UUID.randomUUID().toString().replace("-", ""));
		gg_czyzy.setCzyid(UUID.randomUUID().toString().replace("-", ""));
		gg_czyzy.setZyid(UUID.randomUUID().toString().replace("-", ""));

		assertTrue(this.dao.insert(gg_czyzy) == 1);
	}

	@Test
	public void insert1() {
		TestUserPo testUserPo = new TestUserPo();
		testUserPo.setId(UUID.randomUUID().toString().replace("-", ""));
		testUserPo.setName("王五");
		testUserPo.setAge(20);

		TestUserPo testUserPo1 = new TestUserPo();
		testUserPo1.setId(UUID.randomUUID().toString().replace("-", ""));
		testUserPo1.setName("李四");
		testUserPo1.setAge(20);

		List<BasePo> pos = new ArrayList<>();
		pos.add(testUserPo);
		pos.add(testUserPo1);

		assertTrue(this.dao.insert(pos) == 2);
	}

	@Test
	public void update() {
		TestUserPo testUserPo = new TestUserPo();
		testUserPo.setId("test01");
		testUserPo.setName("王五五");
		testUserPo.setAge(20);

		assertTrue(this.dao.update(testUserPo) == 1);
	}

	@Test
	public void update1() {
		TestUserPo testUserPo = new TestUserPo();
		testUserPo.setId("test01");
		testUserPo.setName("王五五");
		testUserPo.setAge(20);

		TestUserPo testUserPo1 = new TestUserPo();
		testUserPo1.setId("test01");
		testUserPo1.setName("王五");
		testUserPo1.setAge(20);

		List<BasePo> pos = new ArrayList<>();
		pos.add(testUserPo);
		pos.add(testUserPo1);

		assertTrue(this.dao.update(pos) == 2);
	}

	@Test
	public void delete() {
		TestUserPo testUserPo = new TestUserPo();
		testUserPo.setId("0d755ea9eb4941308b23d58746013928");

		this.dao.delete(testUserPo);
	}

	@Test
	public void delete1() {
		this.dao.delete(new TestUserPo(), "name = ?", "李四");
	}

	@Test
	public void get() {
		TestUserPo testUserPo = new TestUserPo();
		testUserPo.setId("test01");

		TestUserPo testUserPo1 = this.dao.get(testUserPo);

		assertEquals(testUserPo1.getName(), "王五五");
	}

	@Test
	public void get1() {
		Map<String, Object> map = this.dao.get("select * from user where id = ?",
				new Object[] { "test01" });

		assertEquals(map.get("name"), "王五五");
	}

	@Test
	public void get2() {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", "test01");

		Map<String, Object> map = this.dao.get("select * from user where id = :id",
				paramMap);

		assertEquals(map.get("name"), "王五五");
	}

	@Test
	public void select() {
		int cn = this.dao.queryForInt("select count(*) from user where name = ?",
				new Object[] { "王五" });

		List<TestUserPo> testUserPos = this.dao.select(new TestUserPo(), "name = ?",
				new Object[] { "王五" });

		assertEquals(cn, testUserPos.size());
	}

	@Test
	public void select1() {
		int cn = this.dao.queryForInt("select count(*) from user where name = ?",
				new Object[] { "王五" });

		List<TestUserPo> testUserPos = this.dao.select(
				"select * from user where name = ?", new Object[] { "王五" },
				TestUserPo.ROW_MAPPER);

		assertEquals(cn, testUserPos.size());
	}

	@Test
	public void select2() {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("name", "王五");

		int cn = this.dao.queryForInt("select count(*) from user where name = :name",
				paramMap);

		List<TestUserPo> testUserPos = this.dao.select(new TestUserPo(), "name = :name",
				paramMap);

		assertEquals(cn, testUserPos.size());
	}

	@Test
	public void select3() {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("name", "王五");

		int cn = this.dao.queryForInt("select count(*) from user where name = :name",
				paramMap);

		List<String> testUserPos = this.dao.select(
				"select name from user where name = :name", paramMap, String.class);

		assertEquals(cn, testUserPos.size());
	}

	@Test
	public void select4() {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("name", "王五");

		int cn = this.dao.queryForInt("select count(*) from user where name = :name",
				paramMap);

		List<String> testUserPos = this.dao.select(
				"select name from user where name = :name", paramMap, new PageInfo(),
				String.class);

		assertEquals(cn, testUserPos.size());
	}

	@Test
	public void select5() {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("name", "王五");

		int cn = this.dao.queryForInt("select count(*) from data_role_user", paramMap);

		List<Map<String, Object>> testUserPos = this.dao
				.select("select * from data_role_user", paramMap);

		assertEquals(cn, testUserPos.size());

		logger.info("select5()查询List<Map<String, Object>>结果{}",
				JSONObject.toJSONString(testUserPos));
	}

	@Test
	public void queryForInt() {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("name", "王五");
		this.dao.queryForInt("select count(*) from user where name = :name", paramMap);
	}

	@Test
	public void queryForInt1() {
		this.dao.queryForInt("select count(*) from user where name = ?",
				new Object[] { "王五" });
	}

	@Test
	public void queryForLong() {
		this.dao.queryForLong("select count(*) from user where name = ?",
				new Object[] { "王五" });
	}

	@Test
	public void queryForLong1() {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("name", "王五");
		this.dao.queryForLong("select count(*) from user where name = :name", paramMap);
	}

	@Test
	public void queryForDouble() {
		this.dao.queryForDouble("select count(*) from user where name = ?",
				new Object[] { "王五" });
	}

	@Test
	public void queryForDouble1() {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("name", "王五");
		this.dao.queryForDouble("select count(*) from user where name = :name", paramMap);
	}

	@Test
	public void queryForBigDecimal() {
		BigDecimal b = this.dao.queryForBigDecimal("select age from user where name = ?",
				new Object[] { "王五1" });
		assertNull(b);
	}

	@Test
	public void queryForBigDecimal1() {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("name", "王五");
		this.dao.queryForBigDecimal("select count(*) from user where name = :name",
				paramMap);
	}

	@Test
	public void queryForString() {
		this.dao.queryForString("select count(*) from user where name = ?",
				new Object[] { "王五" });
	}

	@Test
	public void queryForString1() {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("name", "王五");
		this.dao.queryForString("select count(*) from user where name = :name", paramMap);
	}

	@Test
	public void batchUpdate() {
		List<Map<String, Object>> params = new ArrayList<>();

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("age", 22);
		paramMap.put("name", "王五");

		Map<String, Object> paramMap1 = new HashMap<>();
		paramMap1.put("age", 32);
		paramMap1.put("name", "李四");

		params.add(paramMap);
		params.add(paramMap1);

		this.dao.batchUpdate("update user set age = :age where name = :name", params);
	}

}