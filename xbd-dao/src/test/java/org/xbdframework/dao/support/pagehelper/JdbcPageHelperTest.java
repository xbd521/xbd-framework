package org.xbdframework.dao.support.pagehelper;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import org.xbdframework.dao.PageHelper;
import org.xbdframework.dao.core.PageInfo;

public class JdbcPageHelperTest {

    private String sql;

    private String sqlRaw;

    private PageHelper pageHelper;

    private PageInfo pageInfo;

    @Before
    public void setUp() throws Exception {
        pageHelper = new MysqlPageHelper();
        sql = "select * from user where id = :id and name = :name";
        sqlRaw = "select * from user where id = '11' and name = '李四' limit 5";
        pageInfo = new PageInfo(5);
    }

    @Test
    public void getArrayPagedSql() {
        sql = "select * from user where id = ? and name = ?";
        Object[] params = new Object[] { "11", "李四" };
        String sqlResult = this.pageHelper.paged(sql, params, pageInfo)
                .getSql();
        System.out.println(sqlResult);
        Assert.assertEquals(sqlResult, sqlRaw);
    }

    @Test
    public void getMapPagedSql() {
        Map<String, Object> params = new HashMap<>();
        params.put("id", "11");
        params.put("name", "李四");

        String sqlResult = this.pageHelper.paged(sql, params, pageInfo)
                .getSql();
        System.out.println(sqlResult);
        Assert.assertEquals(sqlResult, sqlRaw);
    }

    @Test
    public void getSqlParameterSourcePagedSql() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", "11");
        params.addValue("name", "李四");

        String sqlResult = this.pageHelper.paged(sql, params, pageInfo)
                .getSql();
        System.out.println(sqlResult);
        Assert.assertEquals(sqlResult, sqlRaw);
    }

}