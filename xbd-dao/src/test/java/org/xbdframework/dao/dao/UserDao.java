package org.xbdframework.dao.dao;

import org.xbdframework.dao.BaseDao;

public interface UserDao extends BaseDao {

	void getUser(String id);

}
