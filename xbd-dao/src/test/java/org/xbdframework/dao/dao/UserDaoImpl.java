package org.xbdframework.dao.dao;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.xbdframework.dao.support.JdbcDaoImpl;

public class UserDaoImpl extends JdbcDaoImpl implements UserDao {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void getUser(String id) {
		String sql = "select * from user where id = ?";

		Map<String, Object> map = get("select * from user where id = ?",
				new Object[] { id });

		System.out.println(map.toString());
	}

}
