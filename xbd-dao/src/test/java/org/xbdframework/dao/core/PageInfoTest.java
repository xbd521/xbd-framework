package org.xbdframework.dao.core;

import org.junit.Assert;
import org.junit.Test;

public class PageInfoTest {

	@Test
	public void testSizeOfLastPage() {
		PageInfo pageInfo = new PageInfo();
		pageInfo.setPageNum(6);
		pageInfo.setTotal(53);

		Assert.assertTrue(pageInfo.getSize() == 3);
	}

	@Test
	public void testSizeOfMiddlePage() {
		PageInfo pageInfo = new PageInfo();
		pageInfo.setPageNum(5);
		pageInfo.setTotal(53);

		Assert.assertTrue(pageInfo.getSize() == PageInfo.PAGESIZE_DEFAULT);
	}

	@Test
	public void testStartRowAndEndRow() {
		PageInfo pageInfo = new PageInfo(1);
		pageInfo.setPageNum(2);
		pageInfo.setTotal(53);

		System.out.println(pageInfo.toString());
		Assert.assertTrue(pageInfo.getStartRow() == 1);
		Assert.assertTrue(pageInfo.getEndRow() == 2);
	}

}