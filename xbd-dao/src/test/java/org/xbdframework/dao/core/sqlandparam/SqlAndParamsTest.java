package org.xbdframework.dao.core.sqlandparam;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import org.xbdframework.dao.core.TestUserPo;
import org.xbdframework.jdbc.core.SqlAndParam;

public class SqlAndParamsTest {

	private TestUserPo testUserPo = new TestUserPo();

	private SqlAndParam<Map<String, Object>> sb;

	private String where = "test_colum = :test_colum";

	private Map<String, Object> params = new HashMap<>();

	@Before
	public void setUp() throws Exception {
		testUserPo.setId("test01");
		testUserPo.setName("王五");
		testUserPo.setAge(20);

		params.put("test_colum", "test_colum");
	}

	@Test
	public void getInsertSql() {
		sb = testUserPo.getInsertSql();

		System.out.println("insert sql : " + sb.getSql());
		System.out.println("insert params : " + sb.getParam().toString());
		System.out.println("insert sql : " + sb.getSql());
	}

	@Test
	public void getUpdateSql() {
		sb = testUserPo.getUpdateSql();

		System.out.println("update sql : " + sb.getSql());
		System.out.println("update params : " + sb.getParam().toString());
		System.out.println("update sql : " + sb.getSql());
	}

	@Test
	public void getDeleteSql() {
		sb = testUserPo.getDeleteSql();

		System.out.println("delete sql : " + sb.getSql());
		System.out.println("delete params : " + sb.getParam().toString());
		System.out.println("delete params : " + sb.getSql());
	}

	@Test
	public void getUpdateWhereSql() {
		sb = testUserPo.getUpdateSql(where, params);

		System.out.println("update param sql : " + sb.getSql());
		System.out.println("update param params : " + sb.getParam().toString());
		System.out.println("update param sql : " + sb.getSql());
	}

	@Test
	public void getDeleteWhereSql() {
		sb = testUserPo.getDeleteSql(where, params);

		System.out.println("delete param sql : " + sb.getSql());
		System.out.println("delete param params : " + sb.getParam().toString());
		System.out.println("delete param params : " + sb.getSql());
	}

}