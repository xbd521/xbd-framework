package org.xbdframework.dao.core;

import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import org.xbdframework.dao.core.sqlandparam.builder.DeleteSqlAndParamBuilder;
import org.xbdframework.dao.core.sqlandparam.builder.InsertSqlAndParamBuilder;
import org.xbdframework.dao.core.sqlandparam.builder.SelectSqlAndParamBuilder;
import org.xbdframework.dao.core.sqlandparam.builder.UpdateSqlAndParamBuilder;
import org.xbdframework.jdbc.core.SqlAndParam;

public class GG_CZYZY extends BasePo {

	public static final RowMapper<GG_CZYZY> ROW_MAPPER = new BeanPropertyRowMapper<>(
			GG_CZYZY.class);

	private String id = null;

	private boolean isset_id = false;

	private String czyid = null;

	private boolean isset_czyid = false;

	private String zyid = null;

	private boolean isset_zyid = false;

	public GG_CZYZY() {
	}

	public GG_CZYZY(String id) {
		this.setId(id);
	}

	public boolean isEmptyId() {
		return this.id == null || this.id.length() == 0;
	}

	@Override
	public String getTableName() {
		return "GG_CZYZY";
	}

	@Override
	public String getPkName() {
		return "id";
	}

	@Override
	public String getPkValue() {
		return this.getId();
	}

	@Override
	public void setPkValue(Object value) {
		this.setId((String) value);
	}

	@Override
	public SqlAndParam<Map<String, Object>> getSelectSql() {
		SelectSqlAndParamBuilder sb = new SelectSqlAndParamBuilder(this);
		return sb.buildMapSql();
	}

	@Override
	public SqlAndParam<Object[]> getSelectSql(String where, Object[] parameters) {
		SelectSqlAndParamBuilder sb = new SelectSqlAndParamBuilder(this);
		return sb.buildArraySql(where, parameters);
	}

	@Override
	public SqlAndParam<Map<String, Object>> getSelectSql(String where,
			Map<String, Object> parameters) {
		SelectSqlAndParamBuilder sb = new SelectSqlAndParamBuilder(this);
		return sb.buildMapSql(where, parameters);
	}

	@Override
	public SqlAndParam<SqlParameterSource> getSelectSql(String where,
			SqlParameterSource params) {
		SelectSqlAndParamBuilder sb = new SelectSqlAndParamBuilder(this);
		return sb.buildSqlParameterSourceSql(where, params);
	}

	@Override
	public SqlAndParam<Map<String, Object>> getInsertSql() {
		InsertSqlAndParamBuilder ib = new InsertSqlAndParamBuilder(this);

		ib.set("id", this.id);
		ib.set("czyid", this.czyid, this.isset_czyid);
		ib.set("zyid", this.zyid, this.isset_zyid);

		return ib.buildMapSql();
	}

	@Override
	public SqlAndParam<Map<String, Object>> getUpdateSql() {
		UpdateSqlAndParamBuilder ub = new UpdateSqlAndParamBuilder(this);

		ub.set("czyid", this.czyid, this.isset_czyid);
		ub.set("zyid", this.zyid, this.isset_zyid);

		return ub.buildMapSql();
	}

	@Override
	public SqlAndParam<Object[]> getUpdateSql(String where, Object[] parameters) {
		UpdateSqlAndParamBuilder ub = new UpdateSqlAndParamBuilder(this);

		ub.set("czyid", this.czyid, this.isset_czyid);
		ub.set("zyid", this.zyid, this.isset_zyid);

		return ub.buildArraySql(where, parameters);
	}

	@Override
	public SqlAndParam<Map<String, Object>> getUpdateSql(String where,
			Map<String, Object> parameters) {
		UpdateSqlAndParamBuilder ub = new UpdateSqlAndParamBuilder(this);

		ub.set("czyid", this.czyid, this.isset_czyid);
		ub.set("zyid", this.zyid, this.isset_zyid);

		return ub.buildMapSql(where, parameters);
	}

	@Override
	public SqlAndParam<SqlParameterSource> getUpdateSql(String where,
			SqlParameterSource params) {
		UpdateSqlAndParamBuilder ub = new UpdateSqlAndParamBuilder(this);

		ub.set("czyid", this.czyid, this.isset_czyid);
		ub.set("zyid", this.zyid, this.isset_zyid);

		return ub.buildSqlParameterSourceSql(where, params);
	}

	@Override
	public SqlAndParam<Map<String, Object>> getDeleteSql() {
		DeleteSqlAndParamBuilder db = new DeleteSqlAndParamBuilder(this);
		return db.buildMapSql();
	}

	@Override
	public SqlAndParam<Object[]> getDeleteSql(String where, Object[] parameters) {
		DeleteSqlAndParamBuilder db = new DeleteSqlAndParamBuilder(this);
		return db.buildArraySql(where, parameters);
	}

	@Override
	public SqlAndParam<Map<String, Object>> getDeleteSql(String where,
			Map<String, Object> parameters) {
		DeleteSqlAndParamBuilder db = new DeleteSqlAndParamBuilder(this);
		return db.buildMapSql(where, parameters);
	}

	@Override
	public SqlAndParam<SqlParameterSource> getDeleteSql(String where,
			SqlParameterSource params) {
		DeleteSqlAndParamBuilder db = new DeleteSqlAndParamBuilder(this);
		return db.buildSqlParameterSourceSql(where, params);
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
				.append("id", this.id).toString();
	}

	@Override
	public GG_CZYZY clone() {
		try {
			GG_CZYZY gg_czyzy = (GG_CZYZY) super.clone();

			if (this.isset_id) {
				gg_czyzy.setId(this.getId());
			}

			if (this.isset_czyid) {
				gg_czyzy.setCzyid(this.getCzyid());
			}

			if (this.isset_zyid) {
				gg_czyzy.setZyid(this.getZyid());
			}

			return gg_czyzy;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
		this.isset_id = true;
	}

	public String getCzyid() {
		return this.czyid;
	}

	public void setCzyid(String czyid) {
		this.czyid = czyid;
		this.isset_czyid = true;
	}

	public String getZyid() {
		return this.zyid;
	}

	public void setZyid(String zyid) {
		this.zyid = zyid;
		this.isset_zyid = true;
	}

}