package org.xbdframework.dao.core.sqlandparam;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

import org.junit.Before;
import org.junit.Test;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import org.xbdframework.dao.core.GG_CZYZY;
import org.xbdframework.jdbc.core.SqlAndParam;

public class SqlAndParamsCzyzyTest {

	private GG_CZYZY gg_czyzy = new GG_CZYZY();

	private SqlAndParam<Map<String, Object>> sb;

	private String where = "test_colum = :test_colum";

	private Map<String, Object> params = new HashMap<>();

	private MapSqlParameterSource parameterSource = new MapSqlParameterSource();

	@Before
	public void setUp() throws Exception {
		gg_czyzy.setId("test01");
		gg_czyzy.setCzyid("0011");
		gg_czyzy.setZyid("1122");

		params.put("test_colum", "test_colum");

		parameterSource.addValues(params);

		RowMapper<GG_CZYZY> ROW_MAPPER = new BeanPropertyRowMapper<>(GG_CZYZY.class);
	}

	@Test
	public void getInsertSql() {
		sb = gg_czyzy.getInsertSql();

		System.out.println("insert sql : " + sb.getSql());
		System.out.println("insert params : " + JSONObject.toJSONString(sb.getParam()));
		System.out.println("insert sql : " + sb.getSql());
	}

	@Test
	public void getUpdateSql() {
		sb = gg_czyzy.getUpdateSql();

		System.out.println("update sql : " + sb.getSql());
		System.out.println("update params : " + JSONObject.toJSONString(sb.getParam()));
		System.out.println("update sql : " + sb.getSql());
	}

	@Test
	public void getDeleteSql() {
		sb = gg_czyzy.getDeleteSql();

		System.out.println("delete sql : " + sb.getSql());
		System.out.println("delete params : " + JSONObject.toJSONString(sb.getParam()));
		System.out.println("delete params : " + sb.getSql());
	}

	@Test
	public void getUpdateWhereSql() {
		sb = gg_czyzy.getUpdateSql(where, params);

		System.out.println("update param sql : " + sb.getSql());
		System.out.println(
				"update param params : " + JSONObject.toJSONString(sb.getParam()));
		System.out.println("update param sql : " + sb.getSql());
	}

	@Test
	public void getUpdateSqlParameterSourceWhereSql() {
		SqlAndParam<SqlParameterSource> sb = gg_czyzy.getUpdateSql(where,
				parameterSource);

		System.out.println("update param sql : " + sb.getSql());
		System.out.println(
				"update param params : " + JSONObject.toJSONString(sb.getParam()));
		System.out.println("update param sql : " + sb.getSql());
	}

	@Test
	public void getDeleteWhereSql() {
		sb = gg_czyzy.getDeleteSql(where, params);

		System.out.println("delete param sql : " + sb.getSql());
		System.out.println(
				"delete param params : " + JSONObject.toJSONString(sb.getParam()));
		System.out.println("delete param params : " + sb.getSql());
	}

	@Test
	public void getDeleteSqlParameterSourceWhereSql() {
		SqlAndParam<SqlParameterSource> sb = gg_czyzy.getDeleteSql(where,
				parameterSource);

		System.out.println("delete param sql : " + sb.getSql());
		System.out.println(
				"delete param params : " + JSONObject.toJSONString(sb.getParam()));
		System.out.println("delete param params : " + sb.getSql());
	}

}