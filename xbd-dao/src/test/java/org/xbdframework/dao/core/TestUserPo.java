package org.xbdframework.dao.core;

import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import org.xbdframework.dao.core.sqlandparam.builder.DeleteSqlAndParamBuilder;
import org.xbdframework.dao.core.sqlandparam.builder.InsertSqlAndParamBuilder;
import org.xbdframework.dao.core.sqlandparam.builder.SelectSqlAndParamBuilder;
import org.xbdframework.dao.core.sqlandparam.builder.UpdateSqlAndParamBuilder;
import org.xbdframework.jdbc.core.SqlAndParam;

public class TestUserPo extends BasePo {

	public static final RowMapper<TestUserPo> ROW_MAPPER = new BeanPropertyRowMapper<>(
			TestUserPo.class);

	private String id;

	private boolean isset_id;

	private String name;

	private boolean isset_name;

	private Integer age;

	private boolean isset_age;

	@Override
	public SqlAndParam<Map<String, Object>> getInsertSql() {
		InsertSqlAndParamBuilder ib = new InsertSqlAndParamBuilder(this);
		ib.set("id", this.id);
		ib.set("name", this.name, this.isset_name);
		ib.set("age", this.age, this.isset_age);

		return ib.buildMapSql();
	}

	@Override
	public SqlAndParam<Map<String, Object>> getUpdateSql() {
		UpdateSqlAndParamBuilder ub = new UpdateSqlAndParamBuilder(this);
		ub.set("name", this.name, this.isset_name);
		ub.set("age", this.age, this.isset_age);
		return ub.buildMapSql();
	}

	@Override
	public SqlAndParam<Map<String, Object>> getUpdateSql(String where,
			Map<String, Object> paramMap) {
		UpdateSqlAndParamBuilder ub = new UpdateSqlAndParamBuilder(this);
		ub.set("name", this.name, this.isset_name);
		ub.set("age", this.age, this.isset_age);

		return ub.buildMapSql(where, paramMap);
	}

	@Override
	public SqlAndParam<Object[]> getUpdateSql(String where, Object[] paramArrayOfObject) {
		UpdateSqlAndParamBuilder ub = new UpdateSqlAndParamBuilder(this);
		ub.set("name", this.name, this.isset_name);
		ub.set("age", this.age, this.isset_age);

		return ub.buildArraySql(where, paramArrayOfObject);
	}

	@Override
	public SqlAndParam<SqlParameterSource> getUpdateSql(String where,
			SqlParameterSource params) {
		UpdateSqlAndParamBuilder ub = new UpdateSqlAndParamBuilder(this);
		ub.set("name", this.name, this.isset_name);
		ub.set("age", this.age, this.isset_age);

		return ub.buildSqlParameterSourceSql(where, params);
	}

	@Override
	public SqlAndParam<Map<String, Object>> getDeleteSql() {
		DeleteSqlAndParamBuilder db = new DeleteSqlAndParamBuilder(this);
		return db.buildMapSql();
	}

	@Override
	public SqlAndParam<Map<String, Object>> getDeleteSql(String where,
			Map<String, Object> paramMap) {
		DeleteSqlAndParamBuilder db = new DeleteSqlAndParamBuilder(this);

		return db.buildMapSql(where, paramMap);
	}

	@Override
	public SqlAndParam<Object[]> getDeleteSql(String where, Object[] paramArrayOfObject) {
		DeleteSqlAndParamBuilder db = new DeleteSqlAndParamBuilder(this);
		return db.buildArraySql(where, paramArrayOfObject);
	}

	@Override
	public SqlAndParam<SqlParameterSource> getDeleteSql(String where,
			SqlParameterSource params) {
		DeleteSqlAndParamBuilder db = new DeleteSqlAndParamBuilder(this);

		return db.buildSqlParameterSourceSql(where, params);
	}

	@Override
	public SqlAndParam<Map<String, Object>> getSelectSql() {
		SelectSqlAndParamBuilder sb = new SelectSqlAndParamBuilder(this);
		return sb.buildMapSql();
	}

	@Override
	public SqlAndParam<Map<String, Object>> getSelectSql(String where,
			Map<String, Object> paramMap) {
		SelectSqlAndParamBuilder sb = new SelectSqlAndParamBuilder(this);
		return sb.buildMapSql(where, paramMap);
	}

	@Override
	public SqlAndParam<Object[]> getSelectSql(String where, Object[] paramArrayOfObject) {
		SelectSqlAndParamBuilder sb = new SelectSqlAndParamBuilder(this);
		return sb.buildArraySql(where, paramArrayOfObject);
	}

	@Override
	public SqlAndParam<SqlParameterSource> getSelectSql(String where,
			SqlParameterSource params) {
		SelectSqlAndParamBuilder sb = new SelectSqlAndParamBuilder(this);
		return sb.buildSqlParameterSourceSql(where, params);
	}

	@Override
	public String getTableName() {
		return "user";
	}

	@Override
	public String getPkName() {
		return "id";
	}

	@Override
	public Object getPkValue() {
		return this.id;
	}

	@Override
	public void setPkValue(Object paramObject) {
		this.id = (String) paramObject;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
		this.isset_id = true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		this.isset_name = true;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
		this.isset_age = true;
	}

}
